//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.utils;

import android.util.Log;
import okhttp3.logging.HttpLoggingInterceptor.Logger;

public class HttpLogger implements Logger {
    private static String TAG = HttpLogger.class.getSimpleName();
    private StringBuilder mMessage = new StringBuilder();

    public HttpLogger() {
    }

    public void log(String message) {
        if (message.startsWith("--> POST")) {
            this.mMessage.setLength(0);
        }

        if (message.startsWith("{") && message.endsWith("}") || message.startsWith("[") && message.endsWith("]")) {
            Log.i(TAG, message);
        }

        this.mMessage.append(message.concat("\n"));
        if (message.startsWith("<-- END HTTP")) {
            Log.i(TAG, this.mMessage.toString());
        }

    }
}