//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.dto;

public class NotificationEventDTO {
    private String title;
    private String message;
    private String topic;
    private String token;
    private String senderId;
    private String senderName;
    private String type;
    private Long actionId;
    private Double totalQuantity;
    private Double totalAmountNext;
    private Long totalQuantityNext;
    private Double incentiveAmount;
    private Double incentivePercent;
    private Double incentiveAmountNext;
    private Double incentivePercentNext;

    public NotificationEventDTO() {
    }

    public String getTitle() {
        return this.title;
    }

    public String getMessage() {
        return this.message;
    }

    public String getTopic() {
        return this.topic;
    }

    public String getToken() {
        return this.token;
    }

    public String getSenderId() {
        return this.senderId;
    }

    public String getSenderName() {
        return this.senderName;
    }

    public String getType() {
        return this.type;
    }

    public Long getActionId() {
        return this.actionId;
    }

    public Double getTotalQuantity() {
        return this.totalQuantity;
    }

    public Double getTotalAmountNext() {
        return this.totalAmountNext;
    }

    public Long getTotalQuantityNext() {
        return this.totalQuantityNext;
    }

    public Double getIncentiveAmount() {
        return this.incentiveAmount;
    }

    public Double getIncentivePercent() {
        return this.incentivePercent;
    }

    public Double getIncentiveAmountNext() {
        return this.incentiveAmountNext;
    }

    public Double getIncentivePercentNext() {
        return this.incentivePercentNext;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }

    public void setTotalQuantity(Double totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public void setTotalAmountNext(Double totalAmountNext) {
        this.totalAmountNext = totalAmountNext;
    }

    public void setTotalQuantityNext(Long totalQuantityNext) {
        this.totalQuantityNext = totalQuantityNext;
    }

    public void setIncentiveAmount(Double incentiveAmount) {
        this.incentiveAmount = incentiveAmount;
    }

    public void setIncentivePercent(Double incentivePercent) {
        this.incentivePercent = incentivePercent;
    }

    public void setIncentiveAmountNext(Double incentiveAmountNext) {
        this.incentiveAmountNext = incentiveAmountNext;
    }

    public void setIncentivePercentNext(Double incentivePercentNext) {
        this.incentivePercentNext = incentivePercentNext;
    }
}