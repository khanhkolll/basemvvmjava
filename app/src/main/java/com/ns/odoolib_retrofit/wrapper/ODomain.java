//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.wrapper;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ODomain extends ODomainArgsHelper<ODomain> {
    public static final String TAG = ODomain.class.getSimpleName();

    public ODomain() {
    }

    public ODomain add(String column, String operator, Object value) {
        JSONArray domain = new JSONArray();
        domain.put(column);
        domain.put(operator);
        if (value instanceof List) {
            domain.put(this.listToArray(value));
        } else {
            domain.put(value);
        }

        this.add(domain);
        return this;
    }

    public JSONObject get() {
        JSONObject result = new JSONObject();

        try {
            result.put("domain", this.getArray());
        } catch (JSONException var3) {
            var3.printStackTrace();
        }

        return result;
    }
}