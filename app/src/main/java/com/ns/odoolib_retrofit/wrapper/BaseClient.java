//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.wrapper;

import android.content.Context;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.adapter.ArrayAdapterFactory;
import com.ns.odoolib_retrofit.adapter.DeserializeOnly;
import com.ns.odoolib_retrofit.adapter.HaiSer;
import com.ns.odoolib_retrofit.model.OdooRelType;
import com.ns.odoolib_retrofit.utils.HttpLogger;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Interceptor.Chain;
import okhttp3.OkHttpClient.Builder;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseClient<Services> {
    protected Retrofit retrofit = null;
    public static String BASE_URL_OAUTH;
    public static String DATE_FORMAT;
    public static String TOKEN = "";
    public static String language = "vi";
    private Services services;
    protected String serverURL;
    protected Gson gson;

    public void reset() {
        this.retrofit = null;
    }

    public void changeLanguage(String lang) {
        this.retrofit = null;
        language = lang;
    }

    public BaseClient(Context context, String baseURL, Class<Services> service) {
        this.serverURL = baseURL;
        this.gson = new Gson();
        this.gson = this.getGsonBuilder().create();
        this.services = this.init(service);
    }

    public static Retrofit getAuthClient() {
        Builder okHttpClientBuilder = new Builder();
        okHttpClientBuilder.addInterceptor(new Interceptor() {
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                okhttp3.Request.Builder newRequest = request.newBuilder().addHeader("Authorization", BaseClient.TOKEN).addHeader("Content-Type", "application/json").addHeader("Accept-Language", BaseClient.language);
                return chain.proceed(newRequest.build());
            }
        });
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat(DATE_FORMAT);
        Retrofit ret = (new retrofit2.Retrofit.Builder()).client(okHttpClientBuilder.build()).baseUrl(BASE_URL_OAUTH).addConverterFactory(GsonConverterFactory.create(gsonBuilder.create())).build();
        return ret;
    }

    public <Services> Services init(Class<Services> services) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLogger());
        interceptor.setLevel(Level.BODY);
        Builder mBuilder = (new Builder()).addInterceptor(interceptor).connectTimeout(30L, TimeUnit.SECONDS).writeTimeout(30L, TimeUnit.SECONDS).readTimeout(30L, TimeUnit.SECONDS).addInterceptor((chain) -> {
            Request request = chain.request();
            okhttp3.Request.Builder newRequest = request.newBuilder().addHeader("Cookie", TOKEN).addHeader("Content-Type", "application/json").addHeader("Accept-Language", "1");
            return chain.proceed(newRequest.build());
        });
        this.retrofit = (new retrofit2.Retrofit.Builder()).baseUrl(this.serverURL).addConverterFactory(GsonConverterFactory.create(this.gson)).client(mBuilder.build()).build();
        return this.retrofit.create(services);
    }

    public <T> T createService(Class<T> service) {
        return this.retrofit.create(service);
    }

    public GsonBuilder getGsonBuilder() {
        GsonBuilder builder = (new GsonBuilder()).registerTypeAdapter(String.class, Adapter.STRING).registerTypeAdapter(Number.class, Adapter.NUMBER).registerTypeAdapter(Integer.class, Adapter.INTEGER).registerTypeAdapter(Long.class, Adapter.LONG).registerTypeAdapter(Float.class, Adapter.FLOAT).registerTypeAdapter(Double.class, Adapter.DOUBLE).registerTypeAdapter(OdooDate.class, Adapter.DATE).registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME).registerTypeAdapter(OdooRelType.class, new HaiSer()).registerTypeAdapterFactory(new ArrayAdapterFactory());
        builder.addSerializationExclusionStrategy(new ExclusionStrategy() {
            public boolean shouldSkipField(FieldAttributes f) {
                return f.getAnnotation(DeserializeOnly.class) != null;
            }

            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        });
        return builder;
    }

    protected String stripURL(String url) {
        if (url != null) {
            String newUrl;
            if (url.endsWith("/")) {
                newUrl = url.substring(0, url.lastIndexOf("/"));
            } else {
                newUrl = url;
            }

            return newUrl;
        } else {
            return null;
        }
    }

    public Services getServices() {
        return this.services;
    }

    public void setServices(Services services) {
        this.services = services;
    }
}