//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.listener;

import retrofit2.Call;
import retrofit2.Response;

public interface ResponseResult<ResponseBody> extends BaseListener {
    void onResponse(Call<ResponseBody> var1, Response<ResponseBody> var2, Object var3, Throwable var4);
}