//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import androidx.annotation.StringRes;

import com.tsolution.base.listener.ViewActionsListener;

public class AlertModel extends BaseModel {
    @StringRes
    public int msg;
    public ViewActionsListener funcPositive;
    public ViewActionsListener funcNegative;

    public AlertModel(int msg, ViewActionsListener funcPositive, ViewActionsListener funcNegative) {
        this.msg = msg;
        this.funcPositive = funcPositive;
        this.funcNegative = funcNegative;
    }

    public int getMsg() {
        return this.msg;
    }

    public void setMsg(int msg) {
        this.msg = msg;
    }

    public ViewActionsListener getFuncPositive() {
        return this.funcPositive;
    }

    public void setFuncPositive(ViewActionsListener funcPositive) {
        this.funcPositive = funcPositive;
    }

    public ViewActionsListener getFuncNegative() {
        return this.funcNegative;
    }

    public void setFuncNegative(ViewActionsListener funcNegative) {
        this.funcNegative = funcNegative;
    }

    public String toString() {
        return "AlertModel(msg=" + this.getMsg() + ", funcPositive=" + this.getFuncPositive() + ", funcNegative=" + this.getFuncNegative() + ")";
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof AlertModel)) {
            return false;
        } else {
            AlertModel other = (AlertModel) o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.getMsg() != other.getMsg()) {
                return false;
            } else {
                Object this$funcPositive = this.getFuncPositive();
                Object other$funcPositive = other.getFuncPositive();
                if (this$funcPositive == null) {
                    if (other$funcPositive != null) {
                        return false;
                    }
                } else if (!this$funcPositive.equals(other$funcPositive)) {
                    return false;
                }

                Object this$funcNegative = this.getFuncNegative();
                Object other$funcNegative = other.getFuncNegative();
                if (this$funcNegative == null) {
                    if (other$funcNegative != null) {
                        return false;
                    }
                } else if (!this$funcNegative.equals(other$funcNegative)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof AlertModel;
    }

    public int hashCode() {
        int result = 1;
        result = result * 59 + this.getMsg();
        Object $funcPositive = this.getFuncPositive();
        result = result * 59 + ($funcPositive == null ? 43 : $funcPositive.hashCode());
        Object $funcNegative = this.getFuncNegative();
        result = result * 59 + ($funcNegative == null ? 43 : $funcNegative.hashCode());
        return result;
    }
}