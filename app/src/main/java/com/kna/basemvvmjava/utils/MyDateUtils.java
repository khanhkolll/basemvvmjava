package com.kna.basemvvmjava.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class MyDateUtils {
    public static final String DATE_SHOW_FORMAT = "dd/MM/yyyy";
    public static final String DATE_FORMAT_DD_MM_YYYY = "dd-MM-yyyy";
    public static final String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String DATE_FORMAT_YYYY_MM_DD_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS'Z'";

    private MyDateUtils() {
    }
    public static Date convertStringToDate(String date, String type) {
        if (ValidateUtils.isNullOrEmpty(date)) {
            return null;
        }
        DateFormat df = new SimpleDateFormat(type);
        Date result = new Date();
        try {
            result = df.parse(date);
            String newDateString = df.format(result);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String convertStringToString(String date, String typeCurrent, String type) {
        if (ValidateUtils.isNullOrEmpty(date)) {
            return "";
        }
        String result;
        SimpleDateFormat formatter = new SimpleDateFormat(type, Locale.US);
        result = formatter.format(convertStringToDate(date,typeCurrent));
        return result;
    }

    public static String convertDateToString(Date date, String type) {
        if (ValidateUtils.isNullOrEmpty(date)) {
            return "";
        }
        String result;
        SimpleDateFormat formatter = new SimpleDateFormat(type, Locale.US);
        result = formatter.format(date);
        return result;
    }


    public static Date convertDayMonthYearToDate(int day, int month, int year) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(year, month, day);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date reduceOneMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);
        return cal.getTime();
    }

}
