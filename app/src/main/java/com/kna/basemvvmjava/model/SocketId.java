package com.kna.basemvvmjava.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SocketId {
    private Integer id;// userId
    private String socketId;
    private String login;//username
}
