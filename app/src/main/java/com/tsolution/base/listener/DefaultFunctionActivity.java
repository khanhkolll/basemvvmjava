//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.listener;

import android.app.ProgressDialog;
import android.content.res.Resources.NotFoundException;
import android.view.View;
import android.view.WindowManager.BadTokenException;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;

import com.kna.basemvvmjava.R;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import java.lang.reflect.Method;

public interface DefaultFunctionActivity extends BaseListener {
    BaseActivity getBaseActivity();

    @LayoutRes
    int getLayoutRes();

    Class<? extends BaseViewModel> getVMClass();

    @IdRes
    int getRecycleResId();

    @IdRes
    default int getXRecycleResId() {
        return this.getRecycleResId();
    }

    default void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        BaseActivity activity = this.getBaseActivity();
        if (t != null) {
            String msg;
            if (t instanceof AppException) {
                msg = ((AppException) t).message;
            } else {
                t.printStackTrace();
                msg = t.getMessage();
            }

            Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
        }

    }

    default void showProcessing(String msg) {
        try {
            BaseActivity activity = this.getBaseActivity();
            ProgressDialog pd = activity.getPd();
            if (pd == null) {
                pd = new ProgressDialog(activity);
                activity.setPd(pd);
            }

            if (!this.getBaseActivity().isFinishing() && !pd.isShowing()) {
                pd.setCancelable(false);
                pd.setMessage(msg);
                pd.show();
            }
        } catch (BadTokenException var4) {
            var4.printStackTrace();
        }

    }

    default void showProcessing(@StringRes int id) {
        String msg = this.getBaseActivity().getResources().getString(id);
        this.showProcessing(msg);
    }

    default void closeAlertDialog() {
        BaseActivity activity = this.getBaseActivity();
        Builder alertDialog = activity.getAlertDialog();
        if (alertDialog != null) {
        }

    }

    default void closeProcess() {
        BaseActivity activity = this.getBaseActivity();
        ProgressDialog pd = activity.getPd();
        if (pd != null) {
            pd.dismiss();
        }

    }

    default void processError(String action, View view, BaseViewModel viewModel, Throwable t) {
        if (t != null) {
            t.printStackTrace();
            boolean sendError = false;
            String msg;
            if (t instanceof AppException) {
                AppException exception = (AppException) t;
                msg = exception.message;

                try {
                    if (exception.code != null) {
                        msg = this.getBaseActivity().getResources().getString(exception.code, new Object[]{exception.message});
                    }
                } catch (NotFoundException var12) {
                    Integer i = (Integer) AppException.errCode.get(exception.code);
                    if (i != null) {
                        try {
                            msg = this.getBaseActivity().getResources().getString(i, new Object[]{exception.message});
                        } catch (NotFoundException var11) {
                        }
                    }
                }
            } else {
                sendError = true;
                t.printStackTrace();
                msg = t.getMessage();
            }

            this.showError(msg, sendError);
            this.closeProcess();
        }
    }

    default void showError(String msg, boolean sendError) {
        BaseActivity activity = this.getBaseActivity();
        Builder alertDialog = activity.getAlertDialog();
        alertDialog = new Builder(activity, R.style.Theme_AppCompat_Dialog_Alert);
        activity.setAlertDialog(alertDialog);
        alertDialog.setMessage(msg);
        alertDialog.setPositiveButton(activity.getResources().getString(R.string.OK), (dialog, which) -> {
            dialog.dismiss();
        });
        if (sendError) {
            alertDialog.setNegativeButton(activity.getResources().getString(R.string.sendError), (dialog, which) -> {
                dialog.dismiss();
            });
        }

        if (!activity.isFinishing()) {
            AlertDialog d = alertDialog.show();
            if (!sendError) {
                Button b = d.getButton(-2);
                b.setVisibility(View.INVISIBLE);
            }
        }

    }

    default void showAlertDialog(@StringRes int msg, AdapterActionsListener action, BaseModel baseModel) {
        BaseActivity activity = this.getBaseActivity();
        Builder alertDialog = activity.getAlertDialog();
        alertDialog = new Builder(activity, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        activity.setAlertDialog(alertDialog);
        alertDialog.setMessage(activity.getResources().getString(msg));
        alertDialog.setPositiveButton(activity.getResources().getString(R.string.OK), (dialog, which) -> {
            dialog.dismiss();
            action.onAdapterClicked((View) null, baseModel);
        });
        alertDialog.setNegativeButton(activity.getResources().getString(R.string.cancel), (dialog, which) -> {
            dialog.dismiss();
        });
        if (!activity.isFinishing()) {
            alertDialog.show();
        }

    }

    default void showAlertDialog(@StringRes int msg, ViewActionsListener action) {
        BaseActivity activity = this.getBaseActivity();
        Builder alertDialog = activity.getAlertDialog();
        alertDialog = new Builder(activity, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        activity.setAlertDialog(alertDialog);
        alertDialog.setMessage(activity.getResources().getString(msg));
        alertDialog.setPositiveButton(activity.getResources().getString(R.string.OK), (dialog, which) -> {
            dialog.dismiss();
            action.onClicked((View) null, (BaseViewModel) null);
        });
        alertDialog.setNegativeButton(activity.getResources().getString(R.string.cancel), (dialog, which) -> {
            dialog.dismiss();
        });
        if (!activity.isFinishing()) {
            alertDialog.show();
        }

    }

    default void showAlertDialog(@StringRes int msg, boolean isCancel) {
        BaseActivity activity = this.getBaseActivity();
        Builder alertDialog = activity.getAlertDialog();
        alertDialog = new Builder(activity, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        activity.setAlertDialog(alertDialog);
        alertDialog.setMessage(activity.getResources().getString(msg));
        alertDialog.setCancelable(isCancel);
        alertDialog.setPositiveButton(activity.getResources().getString(R.string.OK), (dialog, which) -> {
            dialog.dismiss();
        });
        alertDialog.setNegativeButton(activity.getResources().getString(R.string.cancel), (dialog, which) -> {
            dialog.dismiss();
        });
        if (!activity.isFinishing()) {
            alertDialog.show();
        }

    }

    default void showAlertDialog(@StringRes int msg, @StringRes int title, @StringRes int btnOk, @StringRes int btnCancel, ViewActionsListener action) {
        BaseActivity activity = this.getBaseActivity();
        Builder alertDialog = activity.getAlertDialog();
        alertDialog = new Builder(activity, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        activity.setAlertDialog(alertDialog);
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setPositiveButton(activity.getResources().getString(btnOk), (dialog, which) -> {
            dialog.dismiss();
            action.onClicked((View) null, (BaseViewModel) null);
        });
        alertDialog.setNegativeButton(activity.getResources().getString(btnCancel), (dialog, which) -> {
            dialog.dismiss();
        });
        if (!activity.isFinishing()) {
            alertDialog.show();
        }

    }

    default void invokeFunc(String methodName, Object... params) {
        try {
            this.showProcessing(R.string.wait);
            Method method = null;
            Class[] arg = null;
            Method[] methods = this.getClass().getMethods();
            Method[] var6 = methods;
            int var7 = methods.length;

            for (int var8 = 0; var8 < var7; ++var8) {
                Method m = var6[var8];
                if (methodName.equals(m.getName())) {
                    method = m;
                    break;
                }
            }

            if (method == null) {
                throw new NoSuchMethodException(methodName);
            }

            method.invoke(this, params);
        } catch (Throwable var13) {
            this.processError("error", (View) null, (BaseViewModel) null, var13);
        } finally {
            this.closeProcess();
        }

    }
}