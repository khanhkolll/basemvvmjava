//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.model;

public class OdooSessionDto {
    public static final String TAG = OdooSessionDto.class.getSimpleName();
    private String username;
    private String db;
    private Integer uid;
    private boolean is_system;
    private boolean is_admin;
    private String name;
    private String partner_display_name;
    private Integer company_id;
    private Integer partner_id;
    private String show_effect;
    private boolean display_switch_company_menu;
    private String session_id;
    private Integer out_of_office_message;
    private boolean odoobot_initialized;
    private String access_token;
    private Integer distance_notification_message;
    private Integer time_mobile_notification_key;
    private String company_type;
    private Integer save_log_duration;
    private Integer duration_request;
    private Integer distance_check_point;
    private String sharevan_sos;
    private Integer biding_time_confirm;
    private String currency;
    private String google_api_key_geocode;
    private Integer rating_customer_duration_key;

    public OdooSessionDto() {
    }

    public String getUsername() {
        return this.username;
    }

    public String getDb() {
        return this.db;
    }

    public Integer getUid() {
        return this.uid;
    }

    public boolean is_system() {
        return this.is_system;
    }

    public boolean is_admin() {
        return this.is_admin;
    }

    public String getName() {
        return this.name;
    }

    public String getPartner_display_name() {
        return this.partner_display_name;
    }

    public Integer getCompany_id() {
        return this.company_id;
    }

    public Integer getPartner_id() {
        return this.partner_id;
    }

    public String getShow_effect() {
        return this.show_effect;
    }

    public boolean isDisplay_switch_company_menu() {
        return this.display_switch_company_menu;
    }

    public String getSession_id() {
        return this.session_id;
    }

    public Integer getOut_of_office_message() {
        return this.out_of_office_message;
    }

    public boolean isOdoobot_initialized() {
        return this.odoobot_initialized;
    }

    public String getAccess_token() {
        return this.access_token;
    }

    public Integer getDistance_notification_message() {
        return this.distance_notification_message;
    }

    public Integer getTime_mobile_notification_key() {
        return this.time_mobile_notification_key;
    }

    public String getCompany_type() {
        return this.company_type;
    }

    public Integer getSave_log_duration() {
        return this.save_log_duration;
    }

    public Integer getDuration_request() {
        return this.duration_request;
    }

    public Integer getDistance_check_point() {
        return this.distance_check_point;
    }

    public String getSharevan_sos() {
        return this.sharevan_sos;
    }

    public Integer getBiding_time_confirm() {
        return this.biding_time_confirm;
    }

    public String getCurrency() {
        return this.currency;
    }

    public String getGoogle_api_key_geocode() {
        return this.google_api_key_geocode;
    }

    public Integer getRating_customer_duration_key() {
        return this.rating_customer_duration_key;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public void set_system(boolean is_system) {
        this.is_system = is_system;
    }

    public void set_admin(boolean is_admin) {
        this.is_admin = is_admin;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPartner_display_name(String partner_display_name) {
        this.partner_display_name = partner_display_name;
    }

    public void setCompany_id(Integer company_id) {
        this.company_id = company_id;
    }

    public void setPartner_id(Integer partner_id) {
        this.partner_id = partner_id;
    }

    public void setShow_effect(String show_effect) {
        this.show_effect = show_effect;
    }

    public void setDisplay_switch_company_menu(boolean display_switch_company_menu) {
        this.display_switch_company_menu = display_switch_company_menu;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public void setOut_of_office_message(Integer out_of_office_message) {
        this.out_of_office_message = out_of_office_message;
    }

    public void setOdoobot_initialized(boolean odoobot_initialized) {
        this.odoobot_initialized = odoobot_initialized;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public void setDistance_notification_message(Integer distance_notification_message) {
        this.distance_notification_message = distance_notification_message;
    }

    public void setTime_mobile_notification_key(Integer time_mobile_notification_key) {
        this.time_mobile_notification_key = time_mobile_notification_key;
    }

    public void setCompany_type(String company_type) {
        this.company_type = company_type;
    }

    public void setSave_log_duration(Integer save_log_duration) {
        this.save_log_duration = save_log_duration;
    }

    public void setDuration_request(Integer duration_request) {
        this.duration_request = duration_request;
    }

    public void setDistance_check_point(Integer distance_check_point) {
        this.distance_check_point = distance_check_point;
    }

    public void setSharevan_sos(String sharevan_sos) {
        this.sharevan_sos = sharevan_sos;
    }

    public void setBiding_time_confirm(Integer biding_time_confirm) {
        this.biding_time_confirm = biding_time_confirm;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setGoogle_api_key_geocode(String google_api_key_geocode) {
        this.google_api_key_geocode = google_api_key_geocode;
    }

    public void setRating_customer_duration_key(Integer rating_customer_duration_key) {
        this.rating_customer_duration_key = rating_customer_duration_key;
    }
}