//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.dto;

public class EventDTO {
    private String message;
    private int code;

    EventDTO(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public static EventDTO.EventDTOBuilder builder() {
        return new EventDTO.EventDTOBuilder();
    }

    public String getMessage() {
        return this.message;
    }

    public int getCode() {
        return this.code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public static class EventDTOBuilder {
        private String message;
        private int code;

        EventDTOBuilder() {
        }

        public EventDTO.EventDTOBuilder message(String message) {
            this.message = message;
            return this;
        }

        public EventDTO.EventDTOBuilder code(int code) {
            this.code = code;
            return this;
        }

        public EventDTO build() {
            return new EventDTO(this.message, this.code);
        }

        public String toString() {
            return "EventDTO.EventDTOBuilder(message=" + this.message + ", code=" + this.code + ")";
        }
    }
}