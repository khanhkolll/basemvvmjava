//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import java.util.List;

public class ModelRespone<T> {
    private List<T> lstContent;
    private Object pageable;
    private Boolean last;
    private Integer totalPages;
    private Integer totalElements;
    private Integer size;
    private Integer number;
    private Object sort;
    private Boolean first;
    private Boolean empty;
    private Integer numberOfElements;

    public ModelRespone() {
    }

    public List<T> getLstContent() {
        return this.lstContent;
    }

    public Object getPageable() {
        return this.pageable;
    }

    public Boolean getLast() {
        return this.last;
    }

    public Integer getTotalPages() {
        return this.totalPages;
    }

    public Integer getTotalElements() {
        return this.totalElements;
    }

    public Integer getSize() {
        return this.size;
    }

    public Integer getNumber() {
        return this.number;
    }

    public Object getSort() {
        return this.sort;
    }

    public Boolean getFirst() {
        return this.first;
    }

    public Boolean getEmpty() {
        return this.empty;
    }

    public Integer getNumberOfElements() {
        return this.numberOfElements;
    }

    public void setLstContent(List<T> lstContent) {
        this.lstContent = lstContent;
    }

    public void setPageable(Object pageable) {
        this.pageable = pageable;
    }

    public void setLast(Boolean last) {
        this.last = last;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public void setTotalElements(Integer totalElements) {
        this.totalElements = totalElements;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void setSort(Object sort) {
        this.sort = sort;
    }

    public void setFirst(Boolean first) {
        this.first = first;
    }

    public void setEmpty(Boolean empty) {
        this.empty = empty;
    }

    public void setNumberOfElements(Integer numberOfElements) {
        this.numberOfElements = numberOfElements;
    }
}