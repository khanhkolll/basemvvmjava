package com.kna.basemvvmjava.api;

import com.kna.basemvvmjava.base.IResponse;
import com.ns.odoolib_retrofit.listener.IOdooResponse;
import com.workable.errorhandler.ErrorHandler;

public abstract class SharingOdooResponse<Result> implements IOdooResponse<Result>, IResponse<Result> {
    @Override
    public void onResponse(Result result, Throwable volleyError) {
        if (volleyError != null) {
            volleyError.printStackTrace();
            ErrorHandler.create().handle(volleyError);
            onFail(volleyError);
            return;
        }
        onSuccess(result);
    }
}
