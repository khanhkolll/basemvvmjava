//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.HashMap;

public class TsAlerts {
    public TsAlerts() {
    }

    public static void register(Activity activity) {
        TsAlerts.AlertReceiver.register(activity);
    }

    public static void unregister(Activity activity) {
        TsAlerts.AlertReceiver.unregister(activity);
    }

    public static void displayError(Context context, String msg) {
        Intent intent = new Intent("MyApplication.INTENT_DISPLAYERROR");
        intent.putExtra("android.intent.extra.TEXT", msg);
        context.sendOrderedBroadcast(intent, (String)null);
    }

    private static void displayErrorInternal(Context context, String msg) {
        Builder builder = new Builder(context);
        builder.setTitle("Error").setMessage(msg).setCancelable(false).setPositiveButton("Ok", (dialog, id) -> {
            dialog.cancel();
        });
        AlertDialog alert = builder.create();
        if (!((Activity)context).isFinishing()) {
            alert.show();
        }

    }

    private static class AlertReceiver extends BroadcastReceiver {
        private static HashMap<Activity, TsAlerts.AlertReceiver> registrations = new HashMap();
        private Context activityContext;

        private AlertReceiver(Activity activity) {
            this.activityContext = activity;
        }

        static void register(Activity activity) {
            TsAlerts.AlertReceiver receiver = new TsAlerts.AlertReceiver(activity);
            activity.registerReceiver(receiver, new IntentFilter("MyApplication.INTENT_DISPLAYERROR"));
            registrations.put(activity, receiver);
        }

        static void unregister(Activity activity) {
            TsAlerts.AlertReceiver receiver = (TsAlerts.AlertReceiver)registrations.get(activity);
            if (receiver != null) {
                activity.unregisterReceiver(receiver);
                registrations.remove(activity);
            }

        }

        public void onReceive(Context context, Intent intent) {
            this.abortBroadcast();
            String msg = intent.getStringExtra("android.intent.extra.TEXT");
            TsAlerts.displayErrorInternal(this.activityContext, msg);
        }
    }
}