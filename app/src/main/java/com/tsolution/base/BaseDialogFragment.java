//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import android.app.Application;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.LayoutManager;

import com.kna.basemvvmjava.R;
import com.tsolution.base.listener.DefaultFunctionActivity;
import com.tsolution.base.listener.ViewActionsListener;

public abstract class BaseDialogFragment extends DialogFragment implements ViewActionsListener, DefaultFunctionActivity {
    protected RecyclerView recyclerView;
    protected ViewDataBinding binding;
    protected BaseViewModel viewModel;
    protected ProgressDialog pd;

    public BaseDialogFragment() {
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity) this.getActivity();
    }

    @CallSuper
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);

        try {
            this.init(inflater, container, savedInstanceState, this.getLayoutRes(), this.getVMClass(), this.getRecycleResId());
        } catch (Throwable var6) {
            var6.printStackTrace();
        }

        return v;
    }

    public void init(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, @LayoutRes int layoutId, Class<? extends BaseViewModel> clazz, @IdRes int recyclerViewId) throws Exception {
        this.binding = DataBindingUtil.inflate(inflater, layoutId, container, false);
        this.viewModel = (BaseViewModel) clazz.getDeclaredConstructor(Application.class).newInstance(this.getBaseActivity().getApplication());
        View view = this.binding.getRoot();
        this.recyclerView = (RecyclerView) view.findViewById(recyclerViewId);
        LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        this.recyclerView.setLayoutManager(layoutManager);
        this.binding.setVariable(BR.viewModel, this.viewModel);
        this.binding.setVariable(BR.listener, this);
        this.viewModel.setView(this::processFromVM);
        this.viewModel.setAlertModel(this.getBaseActivity().getViewModel().getAlertModel());
    }

    public void action(View view, BaseViewModel baseViewModel) {
        this.showProcessing(this.getResources().getString(R.string.wait));
    }

    public final void onClicked(View view, BaseViewModel viewModel) {
        ViewActionsListener.super.onClicked(view, viewModel);
    }
}