//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import androidx.lifecycle.MutableLiveData;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.tsolution.base.exceptionHandle.AppException;
import com.tsolution.base.listener.ResponseResult;
import java.util.Properties;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyCallBackMapper implements Callback<ResponseBody> {
    private ResponseResult func;
    private Class<?> element;
    private JavaType element1;
    private Class type;
    private MutableLiveData<Throwable> appException;

    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if (response.isSuccessful()) {
            try {
                ResponseBody responseBody = (ResponseBody)response.body();
                if (responseBody != null) {
                    String s = responseBody.string();
                    Object value;
                    if (this.element == null && this.element1 == null) {
                        value = (new Gson()).fromJson(s, Properties.class);
                    } else {
                        ObjectMapper objectMapper = new ObjectMapper();
                        if (this.type == null) {
                            if (this.element1 != null) {
                                value = objectMapper.readValue(s, this.element1);
                            } else {
                                value = objectMapper.readValue(s, this.element);
                            }
                        } else {
                            value = objectMapper.readValue(s, objectMapper.getTypeFactory().constructCollectionType(this.type, this.element));
                        }
                    }

                    if (this.func != null) {
                        this.func.onResponse(call, response, value, (Throwable)null);
                    }
                }
            } catch (Throwable var8) {
                if (this.appException != null) {
                    this.appException.postValue(var8);
                } else if (this.func != null) {
                    this.func.onResponse(call, response, (Object)null, var8);
                }
            }
        } else if (this.appException != null) {
            this.appException.postValue(new AppException(response.code(), response.toString()));
        } else if (this.func != null) {
            this.func.onResponse(call, response, (Object)null, new AppException(response.code(), response.toString()));
        }

    }

    public void onFailure(Call<ResponseBody> call, Throwable t) {
        if (this.appException != null) {
            this.appException.postValue(t);
        } else if (this.func != null) {
            this.func.onResponse(call, (Response)null, (Object)null, t);
        }

    }

    public MyCallBackMapper(MutableLiveData<Throwable> ex, ResponseResult result, Class<?> elem, Class<?> type) {
        this.func = result;
        this.element = elem;
        this.type = type;
        this.appException = ex;
        this.element1 = null;
    }

    public MyCallBackMapper(MutableLiveData<Throwable> ex, ResponseResult result, Class<?> pClass, Class<?> cClass, Class<?> type) {
        this.func = result;
        ObjectMapper objectMapper = new ObjectMapper();
        this.element1 = objectMapper.getTypeFactory().constructParametricType(pClass, new Class[]{cClass});
        this.element = null;
        this.type = type;
        this.appException = ex;
    }

    public MyCallBackMapper(MutableLiveData<Throwable> ex) {
        this.appException = ex;
    }

    public ResponseResult getFunc() {
        return this.func;
    }

    public Class<?> getElement() {
        return this.element;
    }

    public JavaType getElement1() {
        return this.element1;
    }

    public Class getType() {
        return this.type;
    }

    public MutableLiveData<Throwable> getAppException() {
        return this.appException;
    }

    public void setFunc(ResponseResult func) {
        this.func = func;
    }

    public void setElement(Class<?> element) {
        this.element = element;
    }

    public void setElement1(JavaType element1) {
        this.element1 = element1;
    }

    public void setType(Class type) {
        this.type = type;
    }

    public void setAppException(MutableLiveData<Throwable> appException) {
        this.appException = appException;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof MyCallBackMapper)) {
            return false;
        } else {
            MyCallBackMapper other = (MyCallBackMapper)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label71: {
                    Object this$func = this.getFunc();
                    Object other$func = other.getFunc();
                    if (this$func == null) {
                        if (other$func == null) {
                            break label71;
                        }
                    } else if (this$func.equals(other$func)) {
                        break label71;
                    }

                    return false;
                }

                Object this$element = this.getElement();
                Object other$element = other.getElement();
                if (this$element == null) {
                    if (other$element != null) {
                        return false;
                    }
                } else if (!this$element.equals(other$element)) {
                    return false;
                }

                label57: {
                    Object this$element1 = this.getElement1();
                    Object other$element1 = other.getElement1();
                    if (this$element1 == null) {
                        if (other$element1 == null) {
                            break label57;
                        }
                    } else if (this$element1.equals(other$element1)) {
                        break label57;
                    }

                    return false;
                }

                Object this$type = this.getType();
                Object other$type = other.getType();
                if (this$type == null) {
                    if (other$type != null) {
                        return false;
                    }
                } else if (!this$type.equals(other$type)) {
                    return false;
                }

                Object this$appException = this.getAppException();
                Object other$appException = other.getAppException();
                if (this$appException == null) {
                    if (other$appException == null) {
                        return true;
                    }
                } else if (this$appException.equals(other$appException)) {
                    return true;
                }

                return false;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof MyCallBackMapper;
    }

    public int hashCode() {
        int result = 1;
        Object $func = this.getFunc();
        result = result * 59 + ($func == null ? 43 : $func.hashCode());
        Object $element = this.getElement();
        result = result * 59 + ($element == null ? 43 : $element.hashCode());
        Object $element1 = this.getElement1();
        result = result * 59 + ($element1 == null ? 43 : $element1.hashCode());
        Object $type = this.getType();
        result = result * 59 + ($type == null ? 43 : $type.hashCode());
        Object $appException = this.getAppException();
        result = result * 59 + ($appException == null ? 43 : $appException.hashCode());
        return result;
    }

    public String toString() {
        return "MyCallBackMapper(func=" + this.getFunc() + ", element=" + this.getElement() + ", element1=" + this.getElement1() + ", type=" + this.getType() + ", appException=" + this.getAppException() + ")";
    }
}