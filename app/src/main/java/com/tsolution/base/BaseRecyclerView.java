//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.OnScrollListener;
import com.tsolution.base.listener.ILoadMore;

public class BaseRecyclerView extends RecyclerView {
    int lastVisibleItem;
    int totalItemCount;
    int visibleThreshold = 2;
    boolean isLoading;
    ILoadMore loadMore = null;

    public BaseRecyclerView(@NonNull Context context) {
        super(context);
        this.addOnScrollListener(new OnScrollListener() {
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager)recyclerView.getLayoutManager();
                super.onScrolled(recyclerView, dx, dy);
                BaseRecyclerView.this.totalItemCount = linearLayoutManager.getItemCount();
                BaseRecyclerView.this.lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!BaseRecyclerView.this.isLoading && BaseRecyclerView.this.totalItemCount <= BaseRecyclerView.this.lastVisibleItem + BaseRecyclerView.this.visibleThreshold) {
                    if (BaseRecyclerView.this.loadMore != null) {
                        BaseRecyclerView.this.loadMore.onLoadMore();
                    }

                    BaseRecyclerView.this.isLoading = true;
                }

            }
        });
    }

    public void setLoadMore(ILoadMore loadMore) {
        this.loadMore = loadMore;
    }
}