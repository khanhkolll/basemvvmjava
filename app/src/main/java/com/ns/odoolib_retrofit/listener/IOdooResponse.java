//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.listener;

public interface IOdooResponse<T> {
    void onResponse(T var1, Throwable var2);
}