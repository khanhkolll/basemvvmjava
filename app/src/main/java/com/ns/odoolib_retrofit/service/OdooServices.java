//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.service;

import com.google.gson.JsonElement;
import com.ns.odoolib_retrofit.model.OdooResponseDto;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface OdooServices {
    @POST
    Call<OdooResponseDto<JsonElement>> getDataJsonCokies(@Header("Cookie") String var1, @Url String var2, @Body RequestBody var3);
}