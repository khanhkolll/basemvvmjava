//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import android.view.View;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;

public abstract class BaseBinding extends ViewDataBinding {
    protected BaseBinding(DataBindingComponent bindingComponent, View root, int localFieldCount) {
        super(bindingComponent, root, localFieldCount);
    }

    protected BaseBinding(Object bindingComponent, View root, int localFieldCount) {
        super(bindingComponent, root, localFieldCount);
    }

    public void setViewModel(BaseViewModel vm) {
    }
}