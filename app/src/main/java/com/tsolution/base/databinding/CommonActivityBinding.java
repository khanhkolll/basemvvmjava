//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.databinding;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.kna.basemvvmjava.R;
import com.tsolution.base.BaseViewModel;

public abstract class CommonActivityBinding extends ViewDataBinding {
    @NonNull
    public final FrameLayout commonFrag;
    @Bindable
    protected BaseViewModel mViewModel;

    protected CommonActivityBinding(Object _bindingComponent, View _root, int _localFieldCount, FrameLayout commonFrag) {
        super(_bindingComponent, _root, _localFieldCount);
        this.commonFrag = commonFrag;
    }

    @NonNull
    public static CommonActivityBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
    }

    /**
     * @deprecated
     */
    @SuppressLint("RestrictedApi")
    @Deprecated
    @NonNull
    public static CommonActivityBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
        return (CommonActivityBinding) ViewDataBinding.inflateInternal(inflater, R.layout.activity_common, root, attachToRoot, component);
    }

    @NonNull
    public static CommonActivityBinding inflate(@NonNull LayoutInflater inflater) {
        return inflate(inflater, DataBindingUtil.getDefaultComponent());
    }

    /**
     * @deprecated
     */
    @SuppressLint("RestrictedApi")
    @Deprecated
    @NonNull
    public static CommonActivityBinding inflate(@NonNull LayoutInflater inflater, @Nullable Object component) {
        return (CommonActivityBinding) ViewDataBinding.inflateInternal(inflater, R.layout.activity_common, (ViewGroup) null, false, component);
    }

    public static CommonActivityBinding bind(@NonNull View view) {
        return bind(view, DataBindingUtil.getDefaultComponent());
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static CommonActivityBinding bind(@NonNull View view, @Nullable Object component) {
        return (CommonActivityBinding) bind(component, view, R.layout.activity_common);
    }

    @Nullable
    public BaseViewModel getViewModel() {
        return this.mViewModel;
    }

    public abstract void setViewModel(@Nullable BaseViewModel var1);
}