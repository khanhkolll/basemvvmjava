package com.kna.basemvvmjava.base;

public interface IModel {
    String getModelName();
}
