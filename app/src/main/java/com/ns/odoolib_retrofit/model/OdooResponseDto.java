//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.model;

public class OdooResponseDto<Dto> {
    public String jsonrpc;
    public Integer id;
    public Dto result;
    public OdooErrorV2 error;

    public OdooResponseDto() {
    }

    public String toString() {
        return "OdooResponse{jsonrpc='" + this.jsonrpc + '\'' + ", error='" + this.error + '\'' + ", id=" + this.id + ", result=" + this.result + '}';
    }

    public void setJsonrpc(String jsonrpc) {
        this.jsonrpc = jsonrpc;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setResult(Dto result) {
        this.result = result;
    }

    public void setError(OdooErrorV2 error) {
        this.error = error;
    }

    public String getJsonrpc() {
        return this.jsonrpc;
    }

    public Integer getId() {
        return this.id;
    }

    public Dto getResult() {
        return this.result;
    }

    public OdooErrorV2 getError() {
        return this.error;
    }
}