package com.tsolution.base;

import javax.annotation.Generated;

@Generated("Android Data Binding")
public class BR {
    public static final int _all = 0;

    public static final int dialog = 1;

    public static final int listener = 2;

    public static final int listenerAdapter = 3;

    public static final int viewHolder = 4;

    public static final int viewHoler2 = 5;

    public static final int viewModel = 6;
}
