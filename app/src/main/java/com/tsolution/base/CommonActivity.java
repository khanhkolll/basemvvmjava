//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.kna.basemvvmjava.R;
import com.tsolution.base.utils.AlertsUtils;

public class CommonActivity extends BaseActivity {
    private BaseFragment fragment;

    public CommonActivity() {
    }

    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            Class<?> clazz = (Class) this.getIntent().getSerializableExtra("FRAGMENT");
            Fragment fragment;
            if (clazz != null) {
                fragment = (Fragment) clazz.newInstance();
            } else {
                String className = this.getIntent().getStringExtra("FRAGMENT_NAME");
                fragment = (Fragment) ((Fragment) Class.forName(className).newInstance());
            }

            FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.commonFrag, fragment);
            ft.commit();
        } catch (Throwable var5) {
            this.processError("error", (View) null, this.viewModel, var5);
        }

    }

    public int getLayoutRes() {
        return R.layout.activity_common;
    }

    public Class<? extends BaseViewModel> getVMClass() {
        return BaseViewModel.class;
    }

    public int getRecycleResId() {
        return 0;
    }

    public BaseFragment getFragment() {
        return this.fragment;
    }

    public void setFragment(BaseFragment fragment) {
        this.fragment = fragment;
    }

    public void onItemClick(View v, Object o) {
    }

    protected void onResume() {
        super.onResume();
        AlertsUtils.register(this);
    }

    protected void onPause() {
        AlertsUtils.unregister(this);
        super.onPause();
    }
}