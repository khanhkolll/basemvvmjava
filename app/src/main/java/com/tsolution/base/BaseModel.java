//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import java.io.Serializable;

public class BaseModel implements Serializable {
    public transient Boolean checked;
    public transient Integer index;
    public String transactionErrCode;

    public BaseModel() {
    }

    public void bindingAction() {
    }

    public String getIndex() {
        return String.valueOf(this.index);
    }
}