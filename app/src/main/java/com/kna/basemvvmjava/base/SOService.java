package com.kna.basemvvmjava.base;


import com.kna.basemvvmjava.model.ApiResponseModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface SOService {
    @Multipart
    @POST("/share_van_order/accept_package")
    Call<ResponseBody> confirmRouting(
            @Header("Cookie") String cookie,
            @Part("routingPlan") RequestBody body,
            @Part MultipartBody.Part[] files);

    @Multipart
    @POST("/share_van_order/update_routing_plan_day_with_image")
    Call<ApiResponseModel> reportBillPackageChange(
            @Header("Cookie") String cookie,
            @Part("routingPlan") RequestBody body,
            @Part MultipartBody.Part[] files);

    @Multipart
    @POST("/fleet/receive_return_vehicle")
    Call<ApiResponseModel> updateVanStatus(
            @Header("Cookie") String cookie,
            @Part("driverInfo") RequestBody body,
            @Part MultipartBody.Part[] files);

    @Multipart
    @POST("/sos/create")
    Call<ResponseBody> createSOS(
            @Header("Cookie") String cookie,
            @Part("sos_type") RequestBody body,
            @Part("order_number") String orderNumber,
            @Part MultipartBody.Part[] files);

    @Multipart
    @POST("/share_van_order/cancel_routing_once_day")
    Call<ResponseBody> cancelRoutingPlanDay(
            @Header("Cookie") String cookie,
            @Part("bill_lading_detail") Integer bill_lading_detail,
            @Part("description") String description,
            @Part MultipartBody.Part[] files);

    @Multipart
    @POST("/share_van_order/customer_not_found")
    Call<ResponseBody> notContactCustomer(
            @Header("Cookie") String cookie,
            @Part("routing_plan_day_id") RequestBody routing_plan_day_id,
            @Part("type") Integer type,
            @Part("time_pay_back") String time_pay_back,
            @Part MultipartBody.Part[] files);

    @FormUrlEncoded
    @POST("/mobile/logout")
    Call<ResponseBody> logOut(
            @Header("Cookie") String cookie, @Field("user_id") String user_id);
}
