//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.model;

import java.util.List;

public class OdooResultDto<Dto> {
    int length;
    int total_record;
    List<Dto> records;

    public OdooResultDto() {
    }

    public int getLength() {
        return this.length;
    }

    public int getTotal_record() {
        return this.total_record;
    }

    public List<Dto> getRecords() {
        return this.records;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setTotal_record(int total_record) {
        this.total_record = total_record;
    }

    public void setRecords(List<Dto> records) {
        this.records = records;
    }
}