//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.model;

import java.util.Arrays;

public class OdooErrorDetail {
    private String name;
    private String debug;
    private String message;
    private String[] arguments;
    private String exception_type;

    public OdooErrorDetail() {
    }

    public String toString() {
        String debugNew = this.debug == null ? "" : this.debug.replaceAll("\"", "'").replaceAll("\r\n", "\n").replaceAll("\n", " ").replaceAll("\\\\", "/");
        return "{\"name\":\"" + this.name + '"' + ", \"debug\":\"" + debugNew + '"' + ", \"message\":\"" + this.message + '"' + ", \"arguments\":\"" + Arrays.toString(this.arguments).replaceAll("\"", "'") + '"' + ", \"exception_type\":\"" + this.exception_type + '"' + '}';
    }

    public String getName() {
        return this.name;
    }

    public String getDebug() {
        return this.debug;
    }

    public String getMessage() {
        return this.message;
    }

    public String[] getArguments() {
        return this.arguments;
    }

    public String getException_type() {
        return this.exception_type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDebug(String debug) {
        this.debug = debug;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setArguments(String[] arguments) {
        this.arguments = arguments;
    }

    public void setException_type(String exception_type) {
        this.exception_type = exception_type;
    }
}