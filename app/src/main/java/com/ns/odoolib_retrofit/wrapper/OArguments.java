//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.wrapper;

import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OArguments extends ODomainArgsHelper<OArguments> {
    public static final String TAG = OArguments.class.getSimpleName();

    public OArguments() {
    }

    public OArguments add(List<Object> datas) {
        try {
            this.add(new JSONArray(datas.toString()));
        } catch (JSONException var3) {
        }

        return this;
    }

    public OArguments addNULL() {
        this.mObjects.add((Object)null);
        return this;
    }

    public JSONArray get() {
        JSONArray arguments = new JSONArray();
        Iterator var2 = this.getObject().iterator();

        while(var2.hasNext()) {
            Object obj = var2.next();
            JSONArray data = new JSONArray();
            data.put(obj);
            if (obj instanceof JSONObject) {
                arguments.put(obj);
            } else {
                arguments.put(data);
            }
        }

        return arguments;
    }
}