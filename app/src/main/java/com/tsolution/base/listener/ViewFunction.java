//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.listener;

import android.view.View;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;
import com.workable.errorhandler.ErrorHandler;

public interface ViewFunction extends BaseListener {
    void process(String var1, View var2, BaseViewModel var3, Throwable var4);

    default void action(String action, View view, BaseViewModel viewModel, Throwable t) throws AppException {
        try {
            this.process(action, view, viewModel, t);
        } catch (Exception var6) {
            ErrorHandler.create().handle(var6);
        }

    }

    default void action(String action, BaseViewModel viewModel) {
        try {
            this.process(action, (View)null, viewModel, (Throwable)null);
        } catch (Exception var4) {
            ErrorHandler.create().handle(var4);
        }

    }
}