//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.exceptionHandle;

import androidx.lifecycle.MutableLiveData;
import io.reactivex.observers.DisposableObserver;
import java.io.IOException;
import java.net.SocketTimeoutException;
import okhttp3.ResponseBody;
import org.json.JSONObject;
import retrofit2.HttpException;

public abstract class CallbackWrapper<T> extends DisposableObserver<T> {
    private final MutableLiveData<String> responseLV = new MutableLiveData();

    public CallbackWrapper() {
    }

    protected abstract void onSuccess(T var1);

    public void onNext(T t) {
        this.onSuccess(t);
    }

    public void onError(Throwable e) {
        if (e instanceof HttpException) {
            ResponseBody responseBody = ((HttpException)e).response().errorBody();
            this.responseLV.postValue(this.getErrorMessage(responseBody));
            System.out.println("onUnknownError: " + this.getErrorMessage(responseBody));
        } else if (e instanceof SocketTimeoutException) {
            System.out.println("TimeOut: ");
            this.responseLV.postValue("TimeOut");
        } else if (e instanceof IOException) {
            System.out.println("onNetworkError: ");
            this.responseLV.postValue("onNetworkError");
        } else {
            this.responseLV.postValue("onUnknownError");
            System.out.println("onUnknownErrorTryCatch: " + e.getMessage());
        }

    }

    public void onComplete() {
    }

    private String getErrorMessage(ResponseBody responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody.string());
            return jsonObject.getString("message");
        } catch (Exception var3) {
            return var3.getMessage();
        }
    }

    public MutableLiveData<String> getResponseLV() {
        return this.responseLV;
    }
}