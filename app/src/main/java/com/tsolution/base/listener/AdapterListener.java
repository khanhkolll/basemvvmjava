//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.listener;

import android.view.View;

public interface AdapterListener {
    void onItemClick(View var1, Object var2);

    void onItemLongClick(View var1, Object var2);
}