//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class OObjectUtils {
    public OObjectUtils() {
    }

    public static byte[] objectToByte(Object obj) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(outputStream);
        os.writeObject(obj);
        return outputStream.toByteArray();
    }

    public static Object byteToObject(byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(inputStream);
        return is.readObject();
    }

    public static Type getType(Object obj) {
        Type[] types = obj.getClass().getGenericInterfaces();
        Type type;
        if (types != null && types.length > 0) {
            type = types[0];
        } else {
            type = obj.getClass().getGenericSuperclass();
        }

        return type instanceof ParameterizedType ? ((ParameterizedType)type).getActualTypeArguments()[0] : type;
    }
}