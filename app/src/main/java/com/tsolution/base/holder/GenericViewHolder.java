//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.holder;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.tsolution.base.BR;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterActionsListener;

public class GenericViewHolder extends ViewHolder {
    final ViewDataBinding binding;

    public GenericViewHolder(@NonNull View itemView) {
        super(itemView);
        this.binding = null;
    }

    public GenericViewHolder(ViewDataBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setBinding(BaseModel obj, BaseViewModel viewModel, AdapterActionsListener listener) {
        this.binding.setVariable(BR.viewHolder, obj);
        this.binding.setVariable(BR.viewModel, viewModel);
        this.binding.setVariable(BR.listener, listener);
        this.binding.executePendingBindings();
    }

    public void setBinding(BaseModel obj, AdapterActionsListener listener) {
        if (this.binding != null) {
            this.binding.setVariable(BR.viewHolder, obj);
            this.binding.setVariable(BR.listener, listener);
            this.binding.executePendingBindings();
        }
    }
}