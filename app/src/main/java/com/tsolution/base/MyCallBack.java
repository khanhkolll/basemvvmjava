//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import androidx.lifecycle.MutableLiveData;
import com.tsolution.base.exceptionHandle.AppException;
import com.tsolution.base.listener.ResponseResult;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyCallBack<T> implements Callback<T> {
    private ResponseResult func;
    private MutableLiveData<Throwable> appException;

    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            try {
                if (this.func != null) {
                    this.func.onResponse(call, response, response.body(), (Throwable)null);
                }
            } catch (Throwable var5) {
                if (this.appException != null) {
                    this.appException.postValue(var5);
                } else if (this.func != null) {
                    this.func.onResponse(call, response, (Object)null, var5);
                }
            }
        } else if (this.appException != null) {
            try {
                this.appException.postValue(new AppException(response.code(), response.errorBody().string()));
            } catch (IOException var4) {
                var4.printStackTrace();
            }
        } else if (this.func != null) {
            this.func.onResponse(call, response, (Object)null, new AppException(response.code(), response.toString()));
        }

    }

    public void onFailure(Call<T> call, Throwable t) {
        if (this.appException != null) {
            this.appException.postValue(t);
        } else if (this.func != null) {
            this.func.onResponse(call, (Response)null, (Object)null, t);
        }

    }

    public MyCallBack(MutableLiveData<Throwable> ex, ResponseResult result) {
        this.func = result;
        this.appException = ex;
    }

    public MyCallBack(MutableLiveData<Throwable> ex) {
        this.appException = ex;
    }

    public ResponseResult getFunc() {
        return this.func;
    }

    public MutableLiveData<Throwable> getAppException() {
        return this.appException;
    }

    public void setFunc(ResponseResult func) {
        this.func = func;
    }

    public void setAppException(MutableLiveData<Throwable> appException) {
        this.appException = appException;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof MyCallBack)) {
            return false;
        } else {
            MyCallBack<?> other = (MyCallBack)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$func = this.getFunc();
                Object other$func = other.getFunc();
                if (this$func == null) {
                    if (other$func != null) {
                        return false;
                    }
                } else if (!this$func.equals(other$func)) {
                    return false;
                }

                Object this$appException = this.getAppException();
                Object other$appException = other.getAppException();
                if (this$appException == null) {
                    if (other$appException != null) {
                        return false;
                    }
                } else if (!this$appException.equals(other$appException)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof MyCallBack;
    }

    public int hashCode() {
        int result = 1;
        Object $func = this.getFunc();
        result = result * 59 + ($func == null ? 43 : $func.hashCode());
        Object $appException = this.getAppException();
        result = result * 59 + ($appException == null ? 43 : $appException.hashCode());
        return result;
    }

    public String toString() {
        return "MyCallBack(func=" + this.getFunc() + ", appException=" + this.getAppException() + ")";
    }
}