package com.kna.basemvvmjava.api;

import com.kna.basemvvmjava.base.IResponse;
import com.ns.odoolib_retrofit.listener.IOdooResponse;
import com.workable.errorhandler.ErrorHandler;

public abstract class SharingOdooResponse2<T> implements IResponse<T> {

    public IOdooResponse getResponse(Class<T> clazz) {
        return (IOdooResponse<T>) (o, volleyError) -> {
            if (volleyError != null) {
                volleyError.printStackTrace();
                ErrorHandler.create().handle(volleyError);
                return;
            }
            onSuccess(o);
        };
    }

}
