package com.kna.basemvvmjava.base;

public interface RunUi {
    void run(Object... params);
}
