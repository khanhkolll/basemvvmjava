//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import androidx.annotation.NonNull;
import com.tsolution.base.exceptionHandle.AppException;
import com.tsolution.base.listener.ResponseResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyCustomCallBack<T> implements Callback<T> {
    private ResponseResult func;

    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        if (response.isSuccessful()) {
            try {
                if (this.func != null) {
                    this.func.onResponse(call, response, response.body(), (Throwable)null);
                }
            } catch (Throwable var4) {
                if (this.func != null) {
                    this.func.onResponse(call, response, (Object)null, var4);
                }
            }
        } else if (this.func != null) {
            this.func.onResponse(call, response, (Object)null, new AppException(response.code(), response.toString()));
        }

    }

    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        if (this.func != null) {
            this.func.onResponse(call, (Response)null, (Object)null, t);
        }

    }

    public MyCustomCallBack(ResponseResult result) {
        this.func = result;
    }

    public ResponseResult getFunc() {
        return this.func;
    }

    public void setFunc(ResponseResult func) {
        this.func = func;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof MyCustomCallBack)) {
            return false;
        } else {
            MyCustomCallBack<?> other = (MyCustomCallBack)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$func = this.getFunc();
                Object other$func = other.getFunc();
                if (this$func == null) {
                    if (other$func != null) {
                        return false;
                    }
                } else if (!this$func.equals(other$func)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof MyCustomCallBack;
    }

    public int hashCode() {
        int result = 1;
        Object $func = this.getFunc();
        result = result * 59 + ($func == null ? 43 : $func.hashCode());
        return result;
    }

    public String toString() {
        return "MyCustomCallBack(func=" + this.getFunc() + ")";
    }
}