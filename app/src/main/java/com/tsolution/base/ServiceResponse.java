//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class ServiceResponse<T> {
    @SerializedName("content")
    private List<T> arrData;
    @SerializedName("totalPages")
    private Integer totalPages;

    public ServiceResponse() {
    }

    public List<T> getArrData() {
        return this.arrData;
    }

    public Integer getTotalPages() {
        return this.totalPages;
    }

    public void setArrData(List<T> arrData) {
        this.arrData = arrData;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}