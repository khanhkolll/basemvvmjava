//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.databinding;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.kna.basemvvmjava.R;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.BaseListener;

public abstract class BaseLayoutBinding extends ViewDataBinding {
    @Bindable
    protected BaseViewModel mViewModel;
    @Bindable
    protected BaseListener mListener;
    @Bindable
    protected BaseModel mViewHolder;

    protected BaseLayoutBinding(Object _bindingComponent, View _root, int _localFieldCount) {
        super(_bindingComponent, _root, _localFieldCount);
    }

    @NonNull
    public static BaseLayoutBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
    }

    /**
     * @deprecated
     */
    @SuppressLint("RestrictedApi")
    @Deprecated
    @NonNull
    public static BaseLayoutBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
        return (BaseLayoutBinding) ViewDataBinding.inflateInternal(inflater, R.layout.base_layout, root, attachToRoot, component);
    }

    @NonNull
    public static BaseLayoutBinding inflate(@NonNull LayoutInflater inflater) {
        return inflate(inflater, DataBindingUtil.getDefaultComponent());
    }

    /**
     * @deprecated
     */
    @SuppressLint("RestrictedApi")
    @Deprecated
    @NonNull
    public static BaseLayoutBinding inflate(@NonNull LayoutInflater inflater, @Nullable Object component) {
        return (BaseLayoutBinding) ViewDataBinding.inflateInternal(inflater, R.layout.base_layout, (ViewGroup) null, false, component);
    }

    public static BaseLayoutBinding bind(@NonNull View view) {
        return bind(view, DataBindingUtil.getDefaultComponent());
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static BaseLayoutBinding bind(@NonNull View view, @Nullable Object component) {
        return (BaseLayoutBinding) bind(component, view, R.layout.base_layout);
    }

    @Nullable
    public BaseViewModel getViewModel() {
        return this.mViewModel;
    }

    public abstract void setViewModel(@Nullable BaseViewModel var1);

    @Nullable
    public BaseListener getListener() {
        return this.mListener;
    }

    public abstract void setListener(@Nullable BaseListener var1);

    @Nullable
    public BaseModel getViewHolder() {
        return this.mViewHolder;
    }

    public abstract void setViewHolder(@Nullable BaseModel var1);
}