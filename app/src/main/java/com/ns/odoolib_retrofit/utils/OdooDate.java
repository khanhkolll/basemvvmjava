//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.utils;

import java.util.Calendar;
import java.util.Date;

public class OdooDate extends Date {
    private static Calendar calendar = Calendar.getInstance();

    public static void removeTime(Calendar c) {
        c.set(11, 0);
        c.set(12, 0);
        c.set(13, 0);
        c.set(14, 0);
    }

    public OdooDate() {
        calendar.setTime(new Date());
        removeTime(calendar);
        this.setTime(calendar.getTimeInMillis());
    }

    public OdooDate(long date) {
        calendar.setTimeInMillis(date);
        removeTime(calendar);
        this.setTime(calendar.getTimeInMillis());
    }
}