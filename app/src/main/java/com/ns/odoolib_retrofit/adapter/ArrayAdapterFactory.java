//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.adapter;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.MalformedJsonException;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class ArrayAdapterFactory<E> implements TypeAdapterFactory {
    public ArrayAdapterFactory() {
    }

    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
        if (!ArrayList.class.isAssignableFrom(typeToken.getRawType())) {
            return null;
        } else {
            Type elementType = resolveTypeArgument(typeToken.getType());
            TypeAdapter<E> elementTypeAdapter = (TypeAdapter<E>) gson.getAdapter(TypeToken.get(elementType));
            TypeAdapter<T> alwaysListTypeAdapter = (new ArrayAdapterFactory.ArrayAdapter(typeToken, elementTypeAdapter)).nullSafe();
            return alwaysListTypeAdapter;
        }
    }

    private static Type resolveTypeArgument(Type type) {
        if (!(type instanceof ParameterizedType)) {
            return Object.class;
        } else {
            ParameterizedType parameterizedType = (ParameterizedType)type;
            return parameterizedType.getActualTypeArguments()[0];
        }
    }

    private static final class ArrayAdapter<E> extends TypeAdapter<ArrayList<E>> {
        private final TypeAdapter<E> elementTypeAdapter;
        private Class<ArrayList> mClazz;

        private ArrayAdapter(TypeToken typeToken, TypeAdapter<E> elementTypeAdapter) {
            this.elementTypeAdapter = elementTypeAdapter;
            this.mClazz = typeToken.getRawType();
        }

        public void write(JsonWriter out, ArrayList<E> list) throws IOException {
            throw new IOException("not implement");
        }

        public ArrayList<E> read(JsonReader in) throws IOException {
            ArrayList list;
            try {
                list = (ArrayList)this.mClazz.newInstance();
            } catch (Exception var4) {
                throw new AssertionError("Unexpected clazz: " + this.mClazz);
            }

            JsonToken token = in.peek();
            switch(token) {
                case BEGIN_ARRAY:
                    in.beginArray();

                    while(in.hasNext()) {
                        list.add(this.elementTypeAdapter.read(in));
                    }

                    in.endArray();
                    break;
                case BEGIN_OBJECT:
                case STRING:
                case NUMBER:
                case BOOLEAN:
                    list.add(this.elementTypeAdapter.read(in));
                    break;
                case NULL:
                    throw new AssertionError("Must never happen: check if the type adapter configured with .nullSafe()");
                case NAME:
                case END_ARRAY:
                case END_OBJECT:
                case END_DOCUMENT:
                    throw new MalformedJsonException("Unexpected token: " + token);
                default:
                    throw new AssertionError("Must never happen: " + token);
            }

            return list;
        }
    }
}