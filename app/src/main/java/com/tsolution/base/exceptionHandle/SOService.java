//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.exceptionHandle;

import java.util.HashMap;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SOService {
    @POST("crash")
    Call<String> appCrash(@Body AndroidCrashLogDto var1);

    @POST("loadConfig")
    Call<HashMap<String, Object>> loadConfig();
}