//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.wrapper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyCallBack<T> implements Callback<T> {
    public MyCallBack() {
    }

    public void onResponse(Call<T> call, Response<T> response) {
    }

    public void onFailure(Call<T> call, Throwable t) {
    }
}