//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.model;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class OColumn {
    public static final String TAG = OColumn.class.getSimpleName();
    public static final String ROW_ID = "_id";
    private LinkedHashMap<String, String> mSelectionMap;
    private String name;
    private String label;
    private String related_column;
    private Integer size;
    private Class<?> type;
    private OColumn.RelationType relationType;
    private Object defaultValue;
    private Boolean autoIncrement;
    private Boolean required;
    private Boolean isLocalColumn;
    private LinkedHashMap<String, OColumn.ColumnDomain> columnDomains;
    private Integer condition_operator_index;
    private Integer recordSyncLimit;
    private Method mOnChangeMethod;
    private Boolean mOnChangeBGProcess;
    private Boolean mHasDomainFilterColumn;
    private Boolean is_functional_column;
    private Method functional_method;
    private Boolean use_annotation;
    private Boolean functional_store;
    private String[] functional_store_depends;
    private String syncColumnName;
    private String rel_table_name;
    private String rel_base_column;
    private String rel_relation_column;

    public OColumn(String label, Class<?> type) {
        this.mSelectionMap = new LinkedHashMap();
        this.autoIncrement = false;
        this.required = false;
        this.isLocalColumn = false;
        this.columnDomains = new LinkedHashMap();
        this.condition_operator_index = 0;
        this.recordSyncLimit = 0;
        this.mOnChangeMethod = null;
        this.mOnChangeBGProcess = false;
        this.mHasDomainFilterColumn = false;
        this.is_functional_column = false;
        this.functional_method = null;
        this.use_annotation = true;
        this.functional_store = false;
        this.functional_store_depends = null;
        this.syncColumnName = null;
        this.rel_table_name = null;
        this.rel_base_column = null;
        this.rel_relation_column = null;
        this.label = label;
        this.type = type;
    }

    public OColumn(String label, Class<?> type, OColumn.RelationType relationType) {
        this(label, type);
        this.relationType = relationType;
    }

    public OColumn setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getRecordSyncLimit() {
        return this.recordSyncLimit;
    }

    public OColumn setRecordSyncLimit(Integer recordSyncLimit) {
        this.recordSyncLimit = recordSyncLimit;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public OColumn setLabel(String label) {
        this.label = label;
        return this;
    }

    public String getLabel() {
        return this.label;
    }

    public OColumn.RelationType getRelationType() {
        return this.relationType;
    }

    public OColumn setRelatedColumn(String column) {
        this.related_column = column;
        return this;
    }

    public OColumn setSize(Integer size) {
        this.size = size;
        return this;
    }

    public Integer getSize() {
        return this.size;
    }

    public OColumn setDefaultValue(Object defValue) {
        this.defaultValue = defValue;
        return this;
    }

    public OColumn setRequired() {
        this.required = true;
        return this;
    }

    public boolean isRequired() {
        return this.required;
    }

    public OColumn setAutoIncrement() {
        this.autoIncrement = true;
        return this;
    }

    public OColumn setLocalColumn() {
        this.isLocalColumn = true;
        return this;
    }

    public OColumn setType(Class<?> type) {
        this.type = type;
        return this;
    }

    public Class<?> getType() {
        return this.type;
    }

    public Object getDefaultValue() {
        return this.defaultValue;
    }

    public Boolean isAutoIncrement() {
        return this.autoIncrement;
    }

    public Boolean isLocal() {
        return this.isLocalColumn;
    }

    public String getRelatedColumn() {
        return this.related_column;
    }

    public OColumn addDomain(String column_name, String operator, Object value) {
        this.columnDomains.put(column_name, new OColumn.ColumnDomain(column_name, operator, value));
        return this;
    }

    public OColumn addDomain(String condition_operator) {
        LinkedHashMap var10000 = this.columnDomains;
        StringBuilder var10001 = (new StringBuilder()).append("condition_operator_");
        Integer var2 = this.condition_operator_index;
        Integer var3 = this.condition_operator_index = this.condition_operator_index + 1;
        var10000.put(var10001.append(var2).append(condition_operator).toString(), new OColumn.ColumnDomain(condition_operator));
        return this;
    }

    public LinkedHashMap<String, OColumn.ColumnDomain> getDomains() {
        return this.columnDomains;
    }

    public boolean hasDomainFilterColumn() {
        return this.mHasDomainFilterColumn;
    }

    public OColumn setHasDomainFilterColumn(Boolean domainFilterColumn) {
        this.mHasDomainFilterColumn = domainFilterColumn;
        return this;
    }

    public boolean hasOnChange() {
        return this.mOnChangeMethod != null;
    }

    public Method getOnChangeMethod() {
        return this.mOnChangeMethod;
    }

    public void setOnChangeMethod(Method method) {
        this.mOnChangeMethod = method;
    }

    public Boolean isOnChangeBGProcess() {
        return this.mOnChangeBGProcess;
    }

    public void setOnChangeBGProcess(Boolean process) {
        this.mOnChangeBGProcess = process;
    }

    public void cleanDomains() {
        this.columnDomains.clear();
    }

    public OColumn cloneDomain(LinkedHashMap<String, OColumn.ColumnDomain> domains) {
        this.columnDomains.putAll(domains);
        return this;
    }

    public void setFunctionalStore(Boolean store) {
        this.functional_store = store;
    }

    public Boolean canFunctionalStore() {
        return this.functional_store;
    }

    public OColumn setFunctionalStoreDepends(String[] depends) {
        this.functional_store_depends = depends;
        return this;
    }

    public Boolean isFunctionalColumn() {
        return this.is_functional_column;
    }

    public OColumn setIsFunctionalColumn(Boolean is_functional_column) {
        this.is_functional_column = is_functional_column;
        return this;
    }

    public Method getFunctionalMethod() {
        return this.functional_method;
    }

    public OColumn setFunctionalMethod(Method functional_method) {
        this.functional_method = functional_method;
        return this;
    }

    public List<String> getFunctionalStoreDepends() {
        return (List)(this.functional_store_depends != null ? Arrays.asList(this.functional_store_depends) : new ArrayList());
    }

    public HashMap<String, String> getSelectionMap() {
        return this.mSelectionMap;
    }

    public OColumn addSelection(String key, String value) {
        this.mSelectionMap.put(key, value);
        return this;
    }

    public String getSyncColumnName() {
        return this.syncColumnName;
    }

    public void setSyncColumnName(String syncColumnName) {
        this.syncColumnName = syncColumnName;
    }

    public String getSyncColumn() {
        return this.getSyncColumnName() != null ? this.getSyncColumnName() : this.getName();
    }

    public String getRelTableName() {
        return this.rel_table_name;
    }

    public OColumn setRelTableName(String tableName) {
        this.rel_table_name = tableName;
        return this;
    }

    public String getRelBaseColumn() {
        return this.rel_base_column;
    }

    public OColumn setRelBaseColumn(String rel_base_column) {
        this.rel_base_column = rel_base_column;
        return this;
    }

    public String getRelRelationColumn() {
        return this.rel_relation_column;
    }

    public OColumn setRelRelationColumn(String rel_relation_column) {
        this.rel_relation_column = rel_relation_column;
        return this;
    }

    public String toString() {
        return "OColumn{name='" + this.name + '\'' + ", label='" + this.label + '\'' + ", related_column='" + this.related_column + '\'' + ", size=" + this.size + ", type=" + this.type + ", relationType=" + this.relationType + ", defaultValue=" + this.defaultValue + ", autoIncrement=" + this.autoIncrement + ", required=" + this.required + ", isLocalColumn=" + this.isLocalColumn + ", columnDomains=" + this.columnDomains + ", condition_operator_index=" + this.condition_operator_index + ", recordSyncLimit=" + this.recordSyncLimit + ", mOnChangeMethod=" + this.mOnChangeMethod + ", mOnChangeBGProcess=" + this.mOnChangeBGProcess + ", mHasDomainFilterColumn=" + this.mHasDomainFilterColumn + ", is_functional_column=" + this.is_functional_column + ", functional_method=" + this.functional_method + ", use_annotation=" + this.use_annotation + ", functional_store=" + this.functional_store + ", functional_store_depends=" + Arrays.toString(this.functional_store_depends) + '}';
    }

    public static class ColumnDomain {
        private String column = null;
        private String operator = null;
        private Object value = null;
        private String conditional_operator = null;

        public ColumnDomain(String conditional_operator) {
            this.conditional_operator = conditional_operator;
        }

        public ColumnDomain(String column, String operator, Object value) {
            this.column = column;
            this.operator = operator;
            this.value = value;
        }

        public String getColumn() {
            return this.column;
        }

        public void setColumn(String column) {
            this.column = column;
        }

        public String getOperator() {
            return this.operator;
        }

        public void setOperator(String operator) {
            this.operator = operator;
        }

        public Object getValue() {
            return this.value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public String getConditionalOperator() {
            return this.conditional_operator;
        }

        public void setConditionalOperator(String conditional_operator) {
            this.conditional_operator = conditional_operator;
        }

        public String toString() {
            StringBuffer domain = new StringBuffer();
            domain.append("[");
            if (this.conditional_operator == null) {
                domain.append(this.column);
                domain.append(", ");
                domain.append(this.operator);
                domain.append(", ");
                domain.append(this.value);
            } else {
                domain.append(this.conditional_operator);
            }

            domain.append("]");
            return domain.toString();
        }
    }

    public static enum RelationType {
        OneToMany,
        ManyToOne,
        ManyToMany;

        private RelationType() {
        }
    }
}