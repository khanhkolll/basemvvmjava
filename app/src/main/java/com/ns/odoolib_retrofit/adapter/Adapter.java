//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.adapter;

import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.LazilyParsedNumber;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Adapter {
    public static final TypeAdapter<String> STRING = new TypeAdapter<String>() {
        public String read(JsonReader in) throws IOException {
            JsonToken peek = in.peek();
            if (peek == JsonToken.NULL) {
                in.nextNull();
                return null;
            } else if (peek == JsonToken.BOOLEAN) {
                in.nextBoolean();
                return null;
            } else {
                return in.nextString();
            }
        }

        public void write(JsonWriter out, String value) throws IOException {
            out.value(value);
        }
    };
    public static final TypeAdapter<Number> NUMBER = new TypeAdapter<Number>() {
        public Number read(JsonReader in) throws IOException {
            JsonToken jsonToken = in.peek();
            switch(jsonToken) {
                case NULL:
                    in.nextNull();
                    return null;
                case BOOLEAN:
                    in.nextBoolean();
                    return null;
                case NUMBER:
                case STRING:
                    return new LazilyParsedNumber(in.nextString());
                default:
                    throw new JsonSyntaxException("Expecting number, got: " + jsonToken);
            }
        }

        public void write(JsonWriter out, Number value) throws IOException {
            out.value(value);
        }
    };
    public static final TypeAdapter<Integer> INTEGER = new TypeAdapter<Integer>() {
        public Integer read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            } else if (in.peek() == JsonToken.BOOLEAN) {
                in.nextBoolean();
                return null;
            } else {
                try {
                    return (new Double(in.nextDouble())).intValue();
                } catch (NumberFormatException var3) {
                    throw new JsonSyntaxException(var3);
                }
            }
        }

        public void write(JsonWriter out, Integer value) throws IOException {
            out.value(value);
        }
    };
    public static final TypeAdapter<Long> LONG = new TypeAdapter<Long>() {
        public Long read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            } else if (in.peek() == JsonToken.BOOLEAN) {
                in.nextBoolean();
                return null;
            } else {
                try {
                    return (new Double(in.nextDouble())).longValue();
                } catch (NumberFormatException var3) {
                    throw new JsonSyntaxException(var3);
                }
            }
        }

        public void write(JsonWriter out, Long value) throws IOException {
            out.value(value);
        }
    };
    public static final TypeAdapter<Number> FLOAT = new TypeAdapter<Number>() {
        public Number read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            } else if (in.peek() == JsonToken.BOOLEAN) {
                in.nextBoolean();
                return null;
            } else {
                return (float)in.nextDouble();
            }
        }

        public void write(JsonWriter out, Number value) throws IOException {
            out.value(value);
        }
    };
    public static final TypeAdapter<Number> DOUBLE = new TypeAdapter<Number>() {
        public Number read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            } else if (in.peek() == JsonToken.BOOLEAN) {
                in.nextBoolean();
                return null;
            } else {
                return in.nextDouble();
            }
        }

        public void write(JsonWriter out, Number value) throws IOException {
            out.value(value);
        }
    };
    private static final SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final TypeAdapter<OdooDateTime> DATETIME = new TypeAdapter<OdooDateTime>() {
        public OdooDateTime read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            } else if (in.peek() == JsonToken.BOOLEAN) {
                in.nextBoolean();
                return null;
            } else {
                try {
                    Date utcDate = Adapter.sf.parse(in.nextString());
                    long offset = (long)TimeZone.getDefault().getOffset(utcDate.getTime());
                    return new OdooDateTime(utcDate.getTime() + offset);
                } catch (Exception var5) {
                    throw new IOException(var5);
                }
            }
        }

        public void write(JsonWriter out, OdooDateTime value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else {
                long milisec = value.getTime();
                long offset = (long)TimeZone.getDefault().getOffset(milisec);
                out.value(milisec - offset);
            }
        }
    };
    public static final TypeAdapter<OdooDate> DATE = new TypeAdapter<OdooDate>() {
        public OdooDate read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            } else if (in.peek() == JsonToken.BOOLEAN) {
                in.nextBoolean();
                return null;
            } else {
                try {
                    Date utcDate = Adapter.sf.parse(in.nextString());
                    return new OdooDate(utcDate.getTime());
                } catch (Exception var3) {
                    throw new IOException(var3);
                }
            }
        }

        public void write(JsonWriter out, OdooDate value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else {
                out.value(value.getTime());
            }

        }
    };

    public Adapter() {
    }
}