//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.databinding;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingComponent;

import com.tsolution.base.BR;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.BaseListener;

public class BaseLayoutBindingImpl extends BaseLayoutBinding {
    @Nullable
    private static final IncludedLayouts sIncludes = null;
    @Nullable
    private static final SparseIntArray sViewsWithIds = null;
    @NonNull
    private final LinearLayout mboundView0;
    private long mDirtyFlags;

    public BaseLayoutBindingImpl(@Nullable DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 1, sIncludes, sViewsWithIds));
    }

    private BaseLayoutBindingImpl(DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0);
        this.mDirtyFlags = -1L;
        this.mboundView0 = (LinearLayout) bindings[0];
        this.mboundView0.setTag((Object) null);
        this.setRootTag(root);
        this.invalidateAll();
    }

    public void invalidateAll() {
        synchronized (this) {
            this.mDirtyFlags = 8L;
        }

        this.requestRebind();
    }

    public boolean hasPendingBindings() {
        synchronized (this) {
            return this.mDirtyFlags != 0L;
        }
    }

    public boolean setVariable(int variableId, @Nullable Object variable) {
        boolean variableSet = true;
        if (BR.listener == variableId) {
            this.setListener((BaseListener) variable);
        } else if (BR.viewHolder == variableId) {
            this.setViewHolder((BaseModel) variable);
        } else if (BR.viewModel == variableId) {
            this.setViewModel((BaseViewModel) variable);
        } else {
            variableSet = false;
        }

        return variableSet;
    }

    public void setListener(@Nullable BaseListener Listener) {
        this.mListener = Listener;
    }

    public void setViewHolder(@Nullable BaseModel ViewHolder) {
        this.mViewHolder = ViewHolder;
    }

    public void setViewModel(@Nullable BaseViewModel ViewModel) {
        this.mViewModel = ViewModel;
    }

    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        return false;
    }

    protected void executeBindings() {
        long dirtyFlags = 0L;
        synchronized (this) {
            dirtyFlags = this.mDirtyFlags;
            this.mDirtyFlags = 0L;
        }
    }
}