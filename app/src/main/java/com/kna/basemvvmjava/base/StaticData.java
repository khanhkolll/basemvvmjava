package com.kna.basemvvmjava.base;


import com.ns.odoolib_retrofit.model.OdooSessionDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class StaticData {

    public static final String FRAGMENT = "FRAGMENT";
    public static String sessionCookie;
    public static String TOKEN_DCOM = "";
    private static OdooSessionDto odooSessionDto;

    public static OdooSessionDto getOdooSessionDto() {
        return odooSessionDto;
    }

    public static String getUserLogin() {
        if (odooSessionDto == null) {
            return "";
        }
        return odooSessionDto.getUsername();
    }

}
