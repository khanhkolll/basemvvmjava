//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import com.kna.basemvvmjava.R;
import com.tsolution.base.listener.AdapterListener;
import com.tsolution.base.listener.DefaultFunctionActivity;
import com.tsolution.base.listener.ViewActionsListener;

import java.lang.reflect.InvocationTargetException;

public abstract class BaseActivity<V extends ViewDataBinding> extends AppCompatActivity implements ViewActionsListener, DefaultFunctionActivity, AdapterListener {
    protected static ProgressDialog pd;
    private static androidx.appcompat.app.AlertDialog.Builder alertDialog;
    private static androidx.appcompat.app.AlertDialog.Builder alertDialogV2;
    protected Builder pdb;
    protected BaseViewModel viewModel;
    protected V binding;

    public BaseActivity() {
    }

    public void onItemClick(View v, Object o) {
    }

    public Builder getPdb() {
        return this.pdb;
    }

    public void setPdb(Builder pdb) {
        this.pdb = pdb;
    }

    public ProgressDialog getPd() {
        return pd;
    }

    public void setPd(ProgressDialog pd) {
        BaseActivity.pd = pd;
    }

    public androidx.appcompat.app.AlertDialog.Builder getAlertDialog() {
        return alertDialog;
    }

    public void setAlertDialog(androidx.appcompat.app.AlertDialog.Builder alertDialog) {
        BaseActivity.alertDialog = alertDialog;
    }

    public BaseActivity getBaseActivity() {
        return this;
    }

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            this.viewModel = (BaseViewModel) this.getVMClass().getDeclaredConstructor(Application.class).newInstance(this.getBaseActivity().getApplication());
        } catch (IllegalAccessException var3) {
            var3.printStackTrace();
        } catch (InstantiationException var4) {
            var4.printStackTrace();
        } catch (NoSuchMethodException var5) {
            var5.printStackTrace();
        } catch (InvocationTargetException var6) {
            var6.printStackTrace();
        }

        this.binding = DataBindingUtil.setContentView(this, this.getLayoutRes());
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.viewModel.setView(this::processFromVM);
        this.binding.setVariable(BR.viewModel, this.viewModel);
        this.binding.setVariable(BR.listener, this);
        this.viewModel.initObs();
        this.viewModel.getAlertModel().observe(this, (alert) -> {
            AlertModel a = (AlertModel) alert;
            this.showAlertDialog(a.msg, a.funcPositive);
        });
    }

    public void action(View view, BaseViewModel baseViewModel) {
        this.showProcessing(this.getResources().getString(R.string.wait));
    }

    public BaseViewModel getViewModel() {
        return this.viewModel;
    }

    public Object getListener() {
        return this;
    }

    public final void onClicked(View view, BaseViewModel viewModel) {
        ViewActionsListener.super.onClicked(view, viewModel);
    }

    protected void addFragment(@IdRes int containerViewId, @NonNull Fragment fragment, @NonNull String fragmentTag) {
        this.getSupportFragmentManager().beginTransaction().add(containerViewId, fragment, fragmentTag).disallowAddToBackStack().commit();
    }

    public void replaceFragment(@IdRes int containerViewId, @NonNull Fragment fragment, @NonNull String fragmentTag, @Nullable String backStackStateName) {
        this.getSupportFragmentManager().beginTransaction().replace(containerViewId, fragment, fragmentTag).addToBackStack(backStackStateName).commit();
    }

    public void onBackPressed() {
        super.onBackPressed();
        this.closeProcess();
    }

    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        if ("hideKeyBoard".equals(action)) {
            this.hideKeyBoard();
        }

    }

    public void hideKeyBoard() {
        View view1 = this.getBaseActivity().getCurrentFocus();
        if (view1 != null) {
            InputMethodManager imm = (InputMethodManager) this.getBaseActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view1.getWindowToken(), 0);
        }

    }

    public void showCustomDialog(Context context, String title, String message) {
        alertDialogV2 = new androidx.appcompat.app.AlertDialog.Builder(context);
        alertDialogV2.setTitle(title).setMessage(message).setPositiveButton("Ok", (dialog, which) -> {
        });
        if (!((Activity) context).isFinishing()) {
            this.getBaseActivity().runOnUiThread(() -> {
                alertDialogV2.show();
            });
        }

    }

    public void onItemLongClick(View v, Object o) {
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.binding != null) {
            this.binding.unbind();
        }

    }
}