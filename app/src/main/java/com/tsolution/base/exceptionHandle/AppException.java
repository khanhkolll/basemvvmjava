//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.exceptionHandle;

import com.kna.basemvvmjava.R;

import java.util.HashMap;

public class AppException extends Throwable {
    public static HashMap<Integer, Integer> errCode = new HashMap<Integer, Integer>() {
        {
            this.put(401, R.string.unAuthorized);
            this.put(400, R.string.invalidUserOrPass);
            this.put(404, R.string.notFound);
        }
    };
    public Integer code;
    public String message;

    public AppException(Integer cod, String message) {
        this.code = cod;
        this.message = message;
    }

    public AppException() {
    }
}