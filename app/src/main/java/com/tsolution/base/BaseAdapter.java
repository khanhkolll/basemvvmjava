//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableField;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import com.kna.basemvvmjava.R;
import com.tsolution.base.holder.GenericViewHolder;
import com.tsolution.base.listener.AdapterActionsListener;
import com.tsolution.base.listener.ILoadMore;
import com.tsolution.base.listener.OnBottomReachedListener;
import com.tsolution.base.listener.OwnerView;

import java.util.List;

public class BaseAdapter extends Adapter<GenericViewHolder> implements AdapterActionsListener {
    final int visibleThreshold = 5;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    BaseViewModel viewModel;
    OwnerView listener;
    private int layoutId;
    private BaseActivity activity;
    private ObservableField<List> datas;
    private OnBottomReachedListener onBottomReachedListener;
    private Integer lastItemRefresh = 0;

    public BaseAdapter(@LayoutRes int loId, BaseViewModel vm, BaseActivity a) {
        this.layoutId = loId;
        this.viewModel = vm;
        this.activity = a;
        this.datas = this.viewModel.baseModels;
    }

    public BaseAdapter(@LayoutRes int loId, ObservableField data, BaseActivity a) {
        this.layoutId = loId;
        this.datas = data;
        this.activity = a;
        this.viewModel = new BaseViewModel(a.getApplication());
        this.viewModel.baseModels = data;
    }

    public BaseAdapter(@LayoutRes int layoutId, BaseViewModel viewModel, OwnerView listener, BaseActivity a) {
        this.viewModel = viewModel;
        this.listener = listener;
        this.layoutId = layoutId;
        this.activity = a;
        this.datas = viewModel.baseModels;
    }

    public BaseAdapter(@LayoutRes int layoutId, ObservableField data, OwnerView listener, BaseActivity a) {
        this.viewModel = this.viewModel;
        this.listener = listener;
        this.layoutId = layoutId;
        this.activity = a;
        this.datas = data;
        this.viewModel = new BaseViewModel(a.getApplication());
        this.viewModel.baseModels = data;
    }

    public void replaceData(List<BaseModel> tasks) {
        Integer pos = this.viewModel.getLastPos();
        List<BaseModel> lst = (List) this.viewModel.baseModels.get();
        if (lst != null && lst.size() > pos && lst.get(pos) == null) {
            lst.remove(pos);
        }

        switch (this.viewModel.getUpdateAdappType()) {
            case 0:
                this.lastItemRefresh = this.viewModel.getTotalCount() - 1;
                this.notifyDataSetChanged();
                break;
            case 1:
                if (this.lastItemRefresh == 0) {
                    if (lst != null) {
                        this.lastItemRefresh = lst.size() - 1;
                    }

                    this.notifyDataSetChanged();
                } else {
                    Integer count = this.viewModel.getFetchSize();
                    if (count + this.lastItemRefresh >= lst.size()) {
                        count = lst.size() - this.lastItemRefresh - 3;
                    }

                    this.notifyItemRangeInserted(this.lastItemRefresh + 2, count);
                    this.notifyItemChanged(this.lastItemRefresh + 1);
                    this.lastItemRefresh = lst.size() - 1;
                }
        }

        this.viewModel.setLoadingInfo(false);
    }

    @NonNull
    public GenericViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 1) {
            View view = LayoutInflater.from(this.activity).inflate(R.layout.item_loading, parent, false);
            return new BaseAdapter.LoadingViewHolder(view);
        } else {
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), this.layoutId, parent, false);
            return new GenericViewHolder(binding);
        }
    }

    public void onBindViewHolder(@NonNull GenericViewHolder holder, int position) {
        if (holder instanceof BaseAdapter.LoadingViewHolder) {
            BaseAdapter.LoadingViewHolder loadingViewHolder = (BaseAdapter.LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        } else if (this.datas.get() != null && ((List) this.datas.get()).size() != 0 && position < ((List) this.datas.get()).size()) {
            BaseModel bm = (BaseModel) ((List) this.datas.get()).get(position);
            bm.index = position + 1;
            holder.setBinding(bm, this.viewModel, this);
        }
    }

    private void loadData(ILoadMore loadMore) {
        this.viewModel.setLoadingInfo(true);
        Integer fetchSize = this.viewModel.getFetchSize();
        List lstData = (List) this.viewModel.baseModels.get();
        if (lstData != null && lstData.size() > 0 && this.lastItemRefresh + fetchSize < lstData.size()) {
            this.viewModel.add1Fetch();
            this.replaceData((List) null);
        } else {
            int pos = ((List) this.datas.get()).size();
            ((List) this.datas.get()).add((Object) null);
            this.viewModel.setLastPos(pos);
            if (loadMore != null) {
                loadMore.onLoadMore();
            }

        }
    }

    public int getItemCount() {
        return this.datas.get() == null ? 0 : ((List) this.datas.get()).size();
    }

    public void adapterAction(View view, BaseModel baseModel) {
        if (this.listener != null) {
            this.listener.onClicked(view, baseModel);
        }

    }

    public final void onAdapterClicked(View view, BaseModel bm) {
        AdapterActionsListener.super.onAdapterClicked(view, bm);
    }

    public int getItemViewType(int position) {
        if (this.datas.get() != null && ((List) this.datas.get()).size() != 0 && position < ((List) this.datas.get()).size()) {
            return ((List) this.datas.get()).get(position) == null ? 1 : 0;
        } else {
            return 0;
        }
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public BaseActivity getBaseActivity() {
        return this.activity;
    }

    public int getLayoutRes() {
        return this.layoutId;
    }

    public Class<? extends BaseViewModel> getVMClass() {
        return this.viewModel.getClass();
    }

    public int getRecycleResId() {
        return 0;
    }

    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener) {
        this.onBottomReachedListener = onBottomReachedListener;
    }

    public BaseViewModel getVM() {
        return this.viewModel;
    }

    class LoadingViewHolder extends GenericViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            this.progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
        }
    }
}