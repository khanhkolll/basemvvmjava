package com.kna.basemvvmjava.widget;

import android.os.SystemClock;
import android.view.View;

import androidx.databinding.BindingAdapter;

import com.tsolution.base.listener.AdapterListener;

public abstract class OnSingleClickListener implements View.OnClickListener {


    private static final long MIN_CLICK_INTERVAL = 500;
    private static long current_time = 0l;
    private long mLastClickTime;

    @BindingAdapter({"listener", "object"})
    public static void setOnSingleClickListener(View v, AdapterListener adapterListener, Object o) {
        v.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (checkEnableClick()) {
                    adapterListener.onItemClick(v, o);
                }
            }
        });
    }

    public static boolean checkEnableClick() {
        if (SystemClock.elapsedRealtime() - current_time < 1000) {
            return false;
        }
        current_time = SystemClock.elapsedRealtime();
        return true;
    }

    public abstract void onSingleClick(View v);

    @Override
    public final void onClick(View v) {
//        if (checkPermissionLocation(v)) {
//            return;
//        }
        long currentClickTime = SystemClock.elapsedRealtime();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;
        if (elapsedTime <= MIN_CLICK_INTERVAL)
            return;
        onSingleClick(v);
    }

}