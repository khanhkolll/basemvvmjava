//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.listener;

import android.view.View;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;

public interface AdapterActionsListener extends DefaultFunctionActivity {
    void adapterAction(View var1, BaseModel var2);

    default void onAdapterClicked(View view, BaseModel baseModel) {
        try {
            this.adapterAction(view, baseModel);
        } catch (Throwable var4) {
            this.processError((String)null, view, (BaseViewModel)null, var4);
        }

    }
}