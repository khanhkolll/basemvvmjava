//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import androidx.lifecycle.MutableLiveData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.tsolution.base.listener.ResponseResult;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Interceptor.Chain;
import okhttp3.OkHttpClient.Builder;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit retrofit = null;
    public static String clientId;
    public static String clientSecret;
    public static String BASE_URL_OAUTH;
    public static String BASE_URL;
    public static String DATE_FORMAT;
    public static String TOKEN = "";
    public static String USER_NAME;
    public static String language = "vi";

    public RetrofitClient() {
    }

    public static void reset() {
        retrofit = null;
    }

    public static void changeLanguage(String lang) {
        retrofit = null;
        language = lang;
    }

    public static Retrofit getClient() {
        if (retrofit == null) {
            init(TOKEN);
        }

        return retrofit;
    }

    public static Retrofit getAuthClient() {
        Builder okHttpClientBuilder = new Builder();
        okHttpClientBuilder.addInterceptor(new Interceptor() {
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                okhttp3.Request.Builder newRequest = request.newBuilder().addHeader("Authorization", RetrofitClient.TOKEN).addHeader("Content-Type", "application/json").addHeader("Accept-Language", RetrofitClient.language);
                return chain.proceed(newRequest.build());
            }
        });
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat(DATE_FORMAT);
        Retrofit ret = (new retrofit2.Retrofit.Builder()).client(okHttpClientBuilder.build()).baseUrl(BASE_URL_OAUTH).addConverterFactory(GsonConverterFactory.create(gsonBuilder.create())).build();
        return ret;
    }

    public static Retrofit init(String accessToken) {
        Builder okHttpClientBuilder = new Builder();
        okHttpClientBuilder.addInterceptor((chain) -> {
            Request request = chain.request();
            okhttp3.Request.Builder newRequest = request.newBuilder().addHeader("Authorization", accessToken).addHeader("Content-Type", "application/json").addHeader("Accept-Language", language);
            return chain.proceed(newRequest.build());
        });
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat(DATE_FORMAT);
        retrofit = (new retrofit2.Retrofit.Builder()).baseUrl(BASE_URL).client(okHttpClientBuilder.build()).addConverterFactory(GsonConverterFactory.create(gsonBuilder.create())).build();
        return retrofit;
    }

    public static void requestLogin(final MutableLiveData<Throwable> appException, final String userName, String password, final ResponseResult result) {
        String x = Credentials.basic(clientId, clientSecret);
        okhttp3.FormBody.Builder builder = new okhttp3.FormBody.Builder();
        builder.add("grant_type", "password");
        builder.add("username", userName);
        builder.add("password", password);
        Request request = (new okhttp3.Request.Builder()).url(BASE_URL_OAUTH + "/oauth/token").addHeader("origin", "abc").addHeader("Authorization", x).addHeader("Accept", "application/json, text/plain, */*").addHeader("Content-type", "application/x-www-form-urlencoded").addHeader("Accept-Language", language).post(builder.build()).build();
        Builder builder1 = new Builder();
        builder1.build().newCall(request).enqueue(new Callback() {
            public void onFailure(Call call, IOException e) {
                appException.postValue(e);
            }

            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String token = response.body().string();
                    Type type = (new TypeToken<Map<String, String>>() {
                    }).getType();
                    Gson gson = new Gson();
                    Map<String, String> myMap = (Map)gson.fromJson(token, type);
                    RetrofitClient.TOKEN = (String)myMap.get("token_type") + " " + (String)myMap.get("access_token");
                    RetrofitClient.USER_NAME = userName;
                    RetrofitClient.init(RetrofitClient.TOKEN);
                    result.onResponse((retrofit2.Call)null, (retrofit2.Response)null, token, (Throwable)null);
                } else {
                    result.onResponse((retrofit2.Call)null, (retrofit2.Response)null, (Object)null, (Throwable)null);
                }

            }
        });
    }
}