//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.kna.basemvvmjava.R;
import com.kna.basemvvmjava.utils.TsLog;

public class MainActivity extends AppCompatActivity {
    public MainActivity() {
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);
    }
}