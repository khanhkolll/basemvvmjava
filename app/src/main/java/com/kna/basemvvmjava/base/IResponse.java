package com.kna.basemvvmjava.base;


public interface IResponse<Result>{
    void onSuccess(Result result);
    void onFail(Throwable error);
}
