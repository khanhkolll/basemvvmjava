package com.kna.basemvvmjava.socket_notification;

import android.util.Log;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

public class SocketIO implements SocketIONavigation {
    protected Socket mSocket;

    public SocketIO() {
    }

    public void connect(String uri, String channel, onMessageSocket onMessageSocket) {
        {
            try {
                IO.Options opts = new IO.Options();
                opts.forceNew = true;
                opts.reconnection = false;
                mSocket = IO.socket(uri, opts);
            } catch (URISyntaxException e) {
                Log.e("Socket", "connect: "+e.toString() );
            }
        }
        mSocket.on(channel, args -> {
            Log.e("Socket", "onMessage: SocketIO");
            onMessageSocket.messageSocket(args);
        });
        // connect
        mSocket.connect();
    }

    public void disconnect(String channel) {
        if (mSocket != null) {
            Log.e("Socket", "disconnect: SocketIO");
            mSocket.disconnect();
            mSocket.off(Socket.EVENT_CONNECT);
            mSocket.off(channel);
        }
    }

    public interface onMessageSocket {
        void messageSocket(Object... args);
    }
}
