package com.kna.basemvvmjava.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputLayout;
import com.king.zxing.util.CodeUtils;
import com.kna.basemvvmjava.R;

public class BindingAdapterUtil {

    @BindingAdapter("textChanged")
    public static void quantityPackageChange(EditText et, Integer number) {
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!StringUtils.isNumberInt(et.getText().toString())) {
                    ((TextInputLayout) et.getParent().getParent()).setError(et.getContext().getResources().getString(R.string.error_quantity));
                    return;
                }
                if (Double.parseDouble(et.getText().toString()) > number) {
                    ((TextInputLayout) et.getParent().getParent()).setError(et.getContext().getResources().getString(R.string.error_quantity));
                } else {
                    ((TextInputLayout) et.getParent().getParent()).setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @BindingAdapter({"shareVanRoutingPlan", "quantity"})
    public static void showQrChecked(ImageView view, String status, Integer type) {
        switch (type) {
            case 0:
                if (status.equals("0")) {
                    view.setVisibility(View.VISIBLE);
                } else {
                    view.setVisibility(View.GONE);
                }
                break;
            case 1:
                if (!status.equals("2")) {
                    view.setVisibility(View.VISIBLE);
                } else {
                    view.setVisibility(View.GONE);
                }
                break;
        }
    }


    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView view, String imageUrl) {
        if (StringUtils.isNullOrEmpty(imageUrl)) {
            view.setImageDrawable(view.getContext().getResources().getDrawable(R.drawable.ic_stub));
            return;
        }
        loadImage(view, imageUrl, view.getContext(), null);
    }

    @BindingAdapter("imageUrl_rotate")
    public static void loadImageRotate(ImageView view, String imageUrl) {
        if (StringUtils.isNullOrEmpty(imageUrl)) {
            view.setImageDrawable(view.getContext().getResources().getDrawable(R.drawable.ic_stub));
            return;
        }
        loadImage(view, imageUrl, view.getContext(), true);
    }

    private static void loadImage(ImageView imgReward, String icon, Context context, Boolean isRotate) {
        Glide.with(context)
                .load(icon)
                .dontAnimate()
                .placeholder(R.drawable.ic_stub)
                .error(R.drawable.ic_stub)
                .into(imgReward);
        if (isRotate != null) {
            if (imgReward.getHeight() < imgReward.getWidth() && isRotate) {
                imgReward.setRotation(90);
            }
        }
    }


    @SuppressLint("SetJavaScriptEnabled")
    @BindingAdapter("textHtml")
    public static void textHtml(WebView webview, String description) {
        if (description != null) {
            final WebSettings webSettings = webview.getSettings();
            webSettings.setDefaultFontSize(26);
            webSettings.setDefaultTextEncodingName("utf-8");
            webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
            webSettings.setAppCacheEnabled(false);
            webSettings.setBlockNetworkImage(true);
            webSettings.setLoadsImagesAutomatically(true);
            webSettings.setGeolocationEnabled(false);
            webSettings.setNeedInitialFocus(false);
            webSettings.setSaveFormData(false);

            webSettings.setUseWideViewPort(true);
            webSettings.setLoadWithOverviewMode(true);
            webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
            webSettings.setJavaScriptEnabled(true);
            webSettings.setDomStorageEnabled(true);

            webview.loadData(description, "text/html", "UTF-8");
        }

    }


    @BindingAdapter("genQR")
    public static void genQR(ImageView img, String qr) {
        if (qr == null) return;
        Bitmap bitmap = CodeUtils.createQRCode(qr, 600);
        img.setImageBitmap(bitmap);
    }
}
