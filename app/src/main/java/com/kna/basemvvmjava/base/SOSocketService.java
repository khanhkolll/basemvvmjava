package com.kna.basemvvmjava.base;


import com.kna.basemvvmjava.model.ApiResponseModel;
import com.kna.basemvvmjava.model.SocketId;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface SOSocketService {
    @POST("/setSocketId")
    Call<ApiResponseModel> setSocketId(
            @Header("Authorization") String cookie,
            @Body SocketId socketId);
}
