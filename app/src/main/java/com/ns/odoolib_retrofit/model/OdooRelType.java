//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.model;

import java.util.ArrayList;

public class OdooRelType extends ArrayList {
    public OdooRelType() {
    }

    public void setId(Long id) {
        if (id != null) {
            this.setId(id.toString());
        }
    }

    public void setId(String id) {
        if (this.size() > 0) {
            this.set(0, id);
        } else {
            this.add(id);
        }

    }

    public Long getId() {
        if (this.size() > 0) {
            String id = this.get(0).toString();
            return Double.valueOf(id).longValue();
        } else {
            return null;
        }
    }
}