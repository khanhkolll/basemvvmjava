//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.listener;

import android.view.View;
import com.tsolution.base.BaseViewModel;

public interface ViewActionsListener extends DefaultFunctionActivity {
    void action(View var1, BaseViewModel var2);

    default void onClicked(View view, BaseViewModel viewModel) {
        try {
            this.action(view, viewModel);
        } catch (Throwable var4) {
            this.processError((String)null, view, viewModel, var4);
        }

    }
}