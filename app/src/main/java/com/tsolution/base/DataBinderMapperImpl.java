//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import android.annotation.SuppressLint;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;

import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;

import com.kna.basemvvmjava.R;
import com.tsolution.base.databinding.BaseLayoutBindingImpl;
import com.tsolution.base.databinding.CommonActivityBindingImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressLint("RestrictedApi")
public abstract class DataBinderMapperImpl extends DataBinderMapper {
    private static final int LAYOUT_BASELAYOUT = 1;
    private static final int LAYOUT_COMMONACTIVITY = 2;
    private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(2);

    static {
        INTERNAL_LAYOUT_ID_LOOKUP.put(R.layout.base_layout, 1);
        INTERNAL_LAYOUT_ID_LOOKUP.put(R.layout.activity_common, 2);
    }

    public DataBinderMapperImpl() {
    }

    public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
        int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
        if (localizedLayoutId > 0) {
            Object tag = view.getTag();
            if (tag == null) {
                throw new RuntimeException("view must have a tag");
            }

            switch (localizedLayoutId) {
                case 1:
                    if ("layout/base_layout_0".equals(tag)) {
                        return new BaseLayoutBindingImpl(component, view);
                    }

                    throw new IllegalArgumentException("The tag for base_layout is invalid. Received: " + tag);
                case 2:
                    if ("layout/common_activity_0".equals(tag)) {
                        return new CommonActivityBindingImpl(component, view);
                    }

                    throw new IllegalArgumentException("The tag for common_activity is invalid. Received: " + tag);
            }
        }

        return null;
    }

    public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
        if (views != null && views.length != 0) {
            int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
            if (localizedLayoutId > 0) {
                Object tag = views[0].getTag();
                if (tag == null) {
                    throw new RuntimeException("view must have a tag");
                }
            }

            return null;
        } else {
            return null;
        }
    }

    public int getLayoutId(String tag) {
        if (tag == null) {
            return 0;
        } else {
            Integer tmpVal = (Integer) DataBinderMapperImpl.InnerLayoutIdLookup.sKeys.get(tag);
            return tmpVal == null ? 0 : tmpVal;
        }
    }

    public String convertBrIdToString(int localId) {
        String tmpVal = (String) DataBinderMapperImpl.InnerBrLookup.sKeys.get(localId);
        return tmpVal;
    }

    public List<DataBinderMapper> collectDependencies() {
        ArrayList<DataBinderMapper> result = new ArrayList(1);
        result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
        return result;
    }

    private static class InnerLayoutIdLookup {
        static final HashMap<String, Integer> sKeys = new HashMap(2);

        static {
            sKeys.put("layout/base_layout_0", R.layout.base_layout);
            sKeys.put("layout/common_activity_0", R.layout.activity_common);
        }

        private InnerLayoutIdLookup() {
        }
    }

    private static class InnerBrLookup {
        static final SparseArray<String> sKeys = new SparseArray(4);

        static {
            sKeys.put(0, "_all");
            sKeys.put(1, "listener");
            sKeys.put(2, "viewHolder");
            sKeys.put(3, "viewModel");
        }

        private InnerBrLookup() {
        }
    }
}