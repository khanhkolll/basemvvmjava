//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import com.tsolution.base.listener.ViewFunction;
import com.tsolution.base.utils.Utils;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class BaseViewModel<T extends BaseModel> extends AndroidViewModel {
    protected final Integer CURR_PAGE = 0;
    protected final Integer TOTAL_PAGE = 1;
    protected final Integer IS_LOAD = 3;
    protected final Integer UPDATE_LIST_TYPE = 4;
    protected final Integer LAST_POSITION = 5;
    protected final Integer FETCH_SIZE = 6;
    protected final Integer TOTAL_COUNT = 7;
    protected ObservableField<T> model;
    public ObservableField<List<BaseModel>> baseModels = new ObservableField();
    public HashMap<Integer, Object> modelInfo = new HashMap();
    public ObservableField<ErrMsg> errMsg = new ObservableField();
    protected ViewFunction view;
    protected MutableLiveData<AlertModel> alertModel;
    private CompositeDisposable mCompositeDisposable;

    protected void callApi(Disposable disposable) {
        if (this.mCompositeDisposable == null) {
            this.mCompositeDisposable = new CompositeDisposable();
        }

        this.mCompositeDisposable.add(disposable);
    }

    public void initObs() {
        this.alertModel = new MutableLiveData();
    }

    public BaseViewModel(@NonNull Application application) {
        super(application);
        this.baseModels.set(new ArrayList());
        this.model = new ObservableField();
        this.errMsg = new ObservableField();
    }

    public void clearErro(String errCode) {
        ErrMsg err = (ErrMsg)this.errMsg.get();
        if (err == null) {
            err = new ErrMsg();
            this.errMsg.set(err);
        }

        err.addError(errCode, (String)null);
        this.errMsg.notifyChange();
    }

    public void addError(String errCode, @StringRes int resId, boolean notice) {
        ErrMsg err = (ErrMsg)this.errMsg.get();
        if (err == null) {
            err = new ErrMsg();
            this.errMsg.set(err);
        }

        err.addError(errCode, this.getApplication().getResources().getString(resId));
        if (notice) {
            this.errMsg.notifyChange();
        }

    }

    public void addError(String errCode, String message, boolean notice) {
        ErrMsg err = (ErrMsg)this.errMsg.get();
        if (err == null) {
            err = new ErrMsg();
            this.errMsg.set(err);
        }

        err.addError(errCode, message);
        if (notice) {
            this.errMsg.notifyChange();
        }

    }

    public void addError(String errCode, @StringRes int resId) {
        this.addError(errCode, resId, false);
    }

    public int getCount() {
        return this.baseModels == null ? 0 : ((List)this.baseModels.get()).size();
    }

    public void updateModelInfo(List lst) {
        this.modelInfo.put(this.TOTAL_COUNT, lst.size());
        this.modelInfo.put(this.CURR_PAGE, 0);
        this.modelInfo.put(this.FETCH_SIZE, lst.size());
    }

    public void invokeFunc(String methodName, Object... params) {
        try {
            try {
                Method method = null;
                Class[] arg = null;
                Method[] methods = this.getClass().getMethods();
                Method[] var6 = methods;
                int var7 = methods.length;

                for(int var8 = 0; var8 < var7; ++var8) {
                    Method m = var6[var8];
                    if (methodName.equals(m.getName())) {
                        method = m;
                        break;
                    }
                }

                if (method == null) {
                    throw new NoSuchMethodException(methodName);
                }

                method.invoke(this, params);
            } catch (Throwable var13) {
                var13.printStackTrace();
            }

        } finally {
            ;
        }
    }

    public List getCheckedItems() {
        List<BaseModel> lst = new ArrayList();
        Iterator var2 = ((List)this.baseModels.get()).iterator();

        while(var2.hasNext()) {
            BaseModel bm = (BaseModel)var2.next();
            if (bm.checked != null && bm.checked) {
                lst.add(bm);
            }
        }

        return lst;
    }

    public ObservableField<List<BaseModel>> getModels() {
        return this.baseModels;
    }

    public void setView(ViewFunction function) {
        this.view = function;
    }

    public void init() {
    }

    public ObservableField<T> getModel() {
        return this.model;
    }

    public MutableLiveData<AlertModel> getAlertModel() {
        return this.alertModel;
    }

    public void setAlertModel(MutableLiveData<AlertModel> alertModel) {
        this.alertModel = alertModel;
    }

    public BaseViewModel getViewModel() {
        return this;
    }

    public Object getListener() {
        return this;
    }

    public void setData(List<BaseModel> lst) {
        this.setData(lst, true);
        this.updateModelInfo(lst);
    }

    public void setData(List<BaseModel> lst, boolean clear) {
        this.setData((ObservableField)null, lst, clear, true);
    }

    public void setData(ObservableField datas, List<BaseModel> lst) {
        this.setData(datas, lst, true, true);
    }

    public void setData(ObservableField datas, List<BaseModel> lst, boolean clear, boolean notify) {
        new ArrayList();
        if (datas == null) {
            datas = this.baseModels;
        }

        List list = (List)datas.get();
        if (clear) {
            list.clear();
        }

        list.addAll(lst);
        if (notify) {
            datas.notifyChange();
        }

    }

    public T getModelE() {
        return (T) this.model.get();
    }

    public void setModelE(T value, boolean notice) {
        this.model.set(value);
        if (notice) {
            this.model.notifyChange();
        }

    }

    public void setModelE(T value) {
        this.setModelE(value, true);
    }

    public List<BaseModel> getBaseModelsE() {
        return (List)this.baseModels.get();
    }

    public void setBaseModelsE(List values, boolean notice) {
        this.baseModels.set(values);
        if (notice) {
            this.baseModels.notifyChange();
        }

    }

    public void setBaseModelsE(List values) {
        this.setBaseModelsE(values, true);
    }

    public void setLoadingInfo(boolean load) {
        this.modelInfo.put(this.IS_LOAD, load);
    }

    public Integer getUpdateAdappType() {
        return (Integer)Utils.nvl((Integer)this.modelInfo.get(this.UPDATE_LIST_TYPE), 0);
    }

    public void setUpdateAdappType(Integer type) {
        this.modelInfo.put(this.UPDATE_LIST_TYPE, type);
    }

    public void setLastPos(Integer pos) {
        this.modelInfo.put(this.LAST_POSITION, pos);
    }

    public Integer getLastPos() {
        Integer pos = (Integer)this.modelInfo.get(this.LAST_POSITION);
        return pos == null ? 0 : pos;
    }

    public Integer getNextPage() {
        Integer currPage = (Integer)this.modelInfo.get(this.CURR_PAGE);
        Integer totalPage = (Integer)this.modelInfo.get(this.TOTAL_PAGE);
        if (currPage == null) {
            return 0;
        } else {
            return totalPage != null && currPage < totalPage ? currPage + 1 : -2;
        }
    }

    public void addNewPage(List page, int totalPages) {
        this.modelInfo.put(this.TOTAL_PAGE, totalPages);
        Integer currPage = (Integer)this.modelInfo.get(this.CURR_PAGE);
        currPage = currPage == null ? 0 : Integer.valueOf(currPage + 1);
        this.modelInfo.put(this.CURR_PAGE, currPage);
        this.setData((ObservableField)null, Utils.safe(page), false, false);
        this.add1Fetch();
    }

    public Integer getFetchSize() {
        return (Integer)Utils.nvl((Integer)this.modelInfo.get(this.FETCH_SIZE), 5);
    }

    public void setFetchSize(Integer fetchSize) {
        this.modelInfo.put(this.FETCH_SIZE, fetchSize);
    }

    public Integer getTotalCount() {
        return (Integer)Utils.nvl((Integer)this.modelInfo.get(this.TOTAL_COUNT), 0);
    }

    public void add1Fetch() {
        Integer fetchSize = this.getFetchSize();
        List lst = (List)this.baseModels.get();
        if (lst != null && lst.size() != 0) {
            if (this.getTotalCount() + fetchSize < lst.size()) {
                this.modelInfo.put(this.TOTAL_COUNT, this.getTotalCount() + fetchSize);
            } else {
                this.modelInfo.put(this.TOTAL_COUNT, lst.size());
            }

        }
    }

    public void search() {
    }
}