//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.exceptionHandle;

import android.util.Log;
import androidx.lifecycle.MutableLiveData;
import com.tsolution.base.RetrofitClient;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;

public class ApplicationCrashHandler implements UncaughtExceptionHandler {
    private UncaughtExceptionHandler defaultHandler;
    private String userName;
    protected MutableLiveData<Throwable> appException;
    private static final String TAG = "ApplicationCrashHandler";

    public static void installHandler(MutableLiveData<Throwable> appEx) {
        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof ApplicationCrashHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new ApplicationCrashHandler(appEx));
        }

    }

    private ApplicationCrashHandler(MutableLiveData<Throwable> appEx) {
        this.appException = appEx;
        this.defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
    }

    public void uncaughtException(Thread t, Throwable e) {
        Log.wtf("ApplicationCrashHandler", String.format("Exception: %s\n%s", e.toString(), this.getStackTrace(e)));
        AndroidCrashLogDto dto = new AndroidCrashLogDto(RetrofitClient.USER_NAME, this.getStackTrace(e));
        ((SOService)RetrofitClient.getClient().create(SOService.class)).appCrash(dto);
        this.appException.postValue(e);
        this.defaultHandler.uncaughtException(t, e);
    }

    private String getStackTrace(Throwable e) {
        Writer sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String stacktrace = sw.toString();
        pw.close();
        return stacktrace;
    }
}