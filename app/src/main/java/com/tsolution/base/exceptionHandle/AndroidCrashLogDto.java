//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.exceptionHandle;

import java.util.Date;

public class AndroidCrashLogDto {
    public Long id;
    public String userName;
    public String error;
    public String errorDetail;
    public Date createDate;

    public AndroidCrashLogDto(String userName, String error) {
        this.userName = userName;
        this.error = error;
    }
}