//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import java.util.HashMap;

public class ErrMsg extends BaseModel {
    HashMap<String, String> errorMsg = new HashMap();

    public ErrMsg() {
    }

    public void addError(String code, String msg) {
        if (this.errorMsg == null) {
            this.errorMsg = new HashMap();
        }

        this.errorMsg.put(code, msg);
    }

    public String getError(String code) {
        return this.errorMsg == null ? "" : (String)this.errorMsg.get(code);
    }

    public HashMap<String, String> getErrorMsg() {
        return this.errorMsg;
    }

    public void setErrorMsg(HashMap<String, String> errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String toString() {
        return "ErrMsg(errorMsg=" + this.getErrorMsg() + ")";
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof ErrMsg)) {
            return false;
        } else {
            ErrMsg other = (ErrMsg)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$errorMsg = this.getErrorMsg();
                Object other$errorMsg = other.getErrorMsg();
                if (this$errorMsg == null) {
                    if (other$errorMsg != null) {
                        return false;
                    }
                } else if (!this$errorMsg.equals(other$errorMsg)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof ErrMsg;
    }

    public int hashCode() {
        int result = 1;
        Object $errorMsg = this.getErrorMsg();
        result = result * 59 + ($errorMsg == null ? 43 : $errorMsg.hashCode());
        return result;
    }
}