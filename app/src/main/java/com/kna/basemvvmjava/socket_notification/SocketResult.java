package com.kna.basemvvmjava.socket_notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class SocketResult extends BaseModel {


    @SerializedName("result")
    @Expose
    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    @Setter
    @Getter
    public class Result extends BaseModel {
    }


}
