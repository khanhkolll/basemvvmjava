//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.model;

import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OdooFields {
    public static final String TAG = OdooFields.class.getSimpleName();
    private JSONObject jFields = new JSONObject();

    public OdooFields(List<OColumn> columns) {
        List<String> fields = new ArrayList();
        if (columns != null) {
            Iterator var3 = columns.iterator();

            while(var3.hasNext()) {
                OColumn column = (OColumn)var3.next();
                if (!column.isLocal() && !column.isFunctionalColumn()) {
                    fields.add(column.getName());
                }
            }
        }

        this.addAll((String[])fields.toArray(new String[fields.size()]));
    }

    public OdooFields(String... fields) {
        if (fields != null && fields.length > 0) {
            this.addAll(fields);
        }

    }

    public void addAll(String[] fields) {
        try {
            String[] var2 = fields;
            int var3 = fields.length;

            for(int var4 = 0; var4 < var3; ++var4) {
                String field = var2[var4];
                this.jFields.accumulate("fields", field);
            }

            if (fields.length == 1) {
                this.jFields.accumulate("fields", fields[0]);
            }
        } catch (Exception var6) {
            var6.printStackTrace();
        }

    }

    public JSONArray getArray() {
        if (this.jFields.length() != 0) {
            try {
                return this.jFields.getJSONArray("fields");
            } catch (JSONException var2) {
                Log.d(TAG, var2.getMessage());
            }
        }

        return new JSONArray();
    }

    public JSONObject get() {
        if (this.jFields.length() == 0) {
            try {
                this.jFields.put("fields", new JSONArray());
            } catch (JSONException var2) {
                Log.d(TAG, var2.getMessage());
            }
        }

        return this.jFields;
    }
}