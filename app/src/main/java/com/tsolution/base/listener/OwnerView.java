//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.listener;

import android.view.View;
import com.tsolution.base.BaseModel;

public interface OwnerView extends BaseListener {
    void onClicked(View var1, BaseModel var2);
}