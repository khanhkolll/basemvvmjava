//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.CallSuper;
import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.LayoutManager;

import com.tsolution.base.listener.AdapterActionsListener;
import com.tsolution.base.listener.AdapterListener;
import com.tsolution.base.listener.DefaultFunctionActivity;
import com.tsolution.base.listener.ViewActionsListener;

import org.greenrobot.eventbus.EventBus;

public abstract class BaseFragment<V extends ViewDataBinding> extends Fragment implements DefaultFunctionActivity, AdapterActionsListener, ViewActionsListener, AdapterListener {
    protected RecyclerView recyclerView;
    protected V binding;
    protected BaseViewModel viewModel;

    public BaseFragment() {
    }

    public void unregisterEvent() {
        EventBus.getDefault().unregister(this);
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity) this.getActivity();
    }

    @Nullable
    @CallSuper
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            super.onCreateView(inflater, container, savedInstanceState);
            this.binding = DataBindingUtil.inflate(inflater, this.getLayoutRes(), container, false);
            if (this.getVMClass() != null) {
                this.init(savedInstanceState, this.getLayoutRes(), this.getVMClass(), this.getRecycleResId());
            }
        } catch (Throwable var5) {
            var5.printStackTrace();
        }

        return this.binding.getRoot();
    }

    public void init(@Nullable Bundle savedInstanceState, @LayoutRes int layoutId, Class<? extends BaseViewModel> clazz, @IdRes int recyclerViewId) throws Throwable {
        this.viewModel = (BaseViewModel) clazz.getDeclaredConstructor(Application.class).newInstance(this.getBaseActivity().getApplication());
        View view = this.binding.getRoot();
        this.binding.setVariable(BR.viewModel, this.viewModel);
        this.binding.setVariable(BR.listener, this);
        this.viewModel.setView(this::processFromVM);
        this.viewModel.setAlertModel(this.getBaseActivity().getViewModel().getAlertModel());
        if (recyclerViewId != 0) {
            this.recyclerView = (RecyclerView) view.findViewById(recyclerViewId);
            LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
            this.recyclerView.setLayoutManager(layoutManager);
        }

    }

    public void init(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, @LayoutRes int layoutId, Class<? extends BaseViewModel> clazz, @IdRes int recyclerViewId) throws Exception {
        this.viewModel = (BaseViewModel) clazz.getDeclaredConstructor(Application.class).newInstance(this.getBaseActivity().getApplication());
        View view = this.binding.getRoot();
        this.binding.setVariable(BR.viewModel, this.viewModel);
        this.binding.setVariable(BR.listener, this);
        this.viewModel.setView(this::processFromVM);
        this.viewModel.setAlertModel(this.getBaseActivity().getViewModel().getAlertModel());
        if (recyclerViewId != 0) {
            this.recyclerView = (RecyclerView) view.findViewById(recyclerViewId);
            LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
            this.recyclerView.setLayoutManager(layoutManager);
        }

    }

    public void adapterAction(View view, BaseModel baseModel) {
    }

    public final void onAdapterClicked(View view, BaseModel bm) {
        AdapterActionsListener.super.onAdapterClicked(view, bm);
    }

    public void action(View view, BaseViewModel baseViewModel) {
    }

    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        if ("hideKeyBoard".equals(action)) {
            this.hideKeyBoard();
        }

    }

    private void hideKeyBoard() {
        View view1 = this.getBaseActivity().getCurrentFocus();
        if (view1 != null) {
            InputMethodManager imm = (InputMethodManager) this.getBaseActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view1.getWindowToken(), 0);
        }

    }

    public final void onClicked(View view, BaseViewModel viewModel) {
        ViewActionsListener.super.onClicked(view, viewModel);
    }

    public BaseViewModel getViewModel() {
        return this.viewModel;
    }

    public void setViewModel(BaseViewModel viewModel) {
        this.viewModel = viewModel;
    }

    public void onItemClick(View v, Object o) {
    }

    public void onItemLongClick(View v, Object o) {
    }
}