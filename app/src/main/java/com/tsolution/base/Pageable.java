//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import java.util.List;

public class Pageable<T> {
    private List<T> content;
    private Object pageable;
    private Boolean last;
    private Integer totalPages;
    private Integer totalElements;
    private Integer size;
    private Integer number;
    private Object sort;
    private Boolean first;
    private Boolean empty;
    private Integer numberOfElements;

    public Pageable() {
    }

    public String getTotalPages() {
        return this.totalPages == null ? "" : this.totalPages.toString();
    }

    public List<T> getContent() {
        return this.content;
    }

    public Object getPageable() {
        return this.pageable;
    }

    public Boolean getLast() {
        return this.last;
    }

    public Integer getTotalElements() {
        return this.totalElements;
    }

    public Integer getSize() {
        return this.size;
    }

    public Integer getNumber() {
        return this.number;
    }

    public Object getSort() {
        return this.sort;
    }

    public Boolean getFirst() {
        return this.first;
    }

    public Boolean getEmpty() {
        return this.empty;
    }

    public Integer getNumberOfElements() {
        return this.numberOfElements;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }

    public void setPageable(Object pageable) {
        this.pageable = pageable;
    }

    public void setLast(Boolean last) {
        this.last = last;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public void setTotalElements(Integer totalElements) {
        this.totalElements = totalElements;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void setSort(Object sort) {
        this.sort = sort;
    }

    public void setFirst(Boolean first) {
        this.first = first;
    }

    public void setEmpty(Boolean empty) {
        this.empty = empty;
    }

    public void setNumberOfElements(Integer numberOfElements) {
        this.numberOfElements = numberOfElements;
    }
}