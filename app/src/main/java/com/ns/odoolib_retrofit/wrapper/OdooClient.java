//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.wrapper;

import android.content.Context;
import com.google.gson.JsonElement;
import com.ns.odoolib_retrofit.listener.IOdooResponse;
import com.ns.odoolib_retrofit.model.OdooFields;
import com.ns.odoolib_retrofit.model.OdooResponseDto;
import com.ns.odoolib_retrofit.service.OdooServices;
import com.ns.odoolib_retrofit.utils.OObjectUtils;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Random;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OdooClient {
    BaseClient<OdooServices> client;
    protected String sessionCokie = "";

    public OdooClient(Context context, String baseURL) {
        this.client = new BaseClient(context, baseURL, OdooServices.class);
    }

    public void authenticate(String username, String password, String database, IOdooResponse callback) {
        try {
            String url = "/web/session/authenticate";
            JSONObject params = new JSONObject();
            params.put("db", database);
            params.put("login", username);
            params.put("password", password);
            params.put("context", new JSONObject());
            this.retrofitRequest(url, params, callback);
        } catch (Exception var7) {
            callback.onResponse((Object)null, var7);
        }

    }

    public void callMethod(String model, String method, OArguments arguments, HashMap<String, Object> kwargs, HashMap<String, Object> context, IOdooResponse callback) {
        String url = "/web/dataset/call_kw";

        try {
            JSONObject params = new JSONObject();
            params.put("model", model);
            params.put("method", method);
            params.put("args", arguments.getArray());
            params.put("kwargs", kwargs != null ? new JSONObject(this.client.gson.toJson(kwargs)) : new JSONObject());
            this.retrofitRequest(url, params, callback);
        } catch (Exception var9) {
            callback.onResponse((Object)null, var9);
        }

    }

    private JSONObject createRequestWrapper(JSONObject params) throws Exception {
        JSONObject requestData = new JSONObject();

        try {
            int randomId = this.getRequestID();
            JSONObject newParams = params;
            if (params == null) {
                newParams = new JSONObject();
            }

            requestData.put("jsonrpc", "2.0");
            requestData.put("method", "call");
            requestData.put("params", newParams);
            requestData.put("id", randomId);
            return requestData;
        } catch (Exception var5) {
            throw var5;
        }
    }

    private int getRequestID() {
        return Math.abs((new Random()).nextInt(9999));
    }

    public void searchRead(String model, OdooFields fields, ODomain domain, int offset, int limit, String sort, IOdooResponse callback) {
        try {
            String url = "/web/dataset/search_read";
            JSONObject params = new JSONObject();
            params.put("model", model);
            if (fields == null) {
                fields = new OdooFields(new String[0]);
            }

            params.put("fields", fields.get().getJSONArray("fields"));
            if (domain == null) {
                domain = new ODomain();
            }

            params.put("domain", domain.getArray());
            params.put("offset", offset);
            params.put("limit", limit);
            params.put("sort", sort == null ? "" : sort);
            this.retrofitRequest(url, params, callback);
        } catch (Exception var10) {
            callback.onResponse((Object)null, var10);
        }

    }

    public void callRoute(String route, JSONObject params, IOdooResponse odooResponse) throws Exception {
        this.retrofitRequest(route, params, odooResponse);
    }

    public void retrofitRequest(String url, JSONObject params, IOdooResponse odooResponse) throws Exception {
        this.retrofitRequest(url, "application/json", params, odooResponse);
    }

    public void retrofitRequest(String url, String mediaTye, JSONObject params, final IOdooResponse odooResponse) throws Exception {
        JSONObject postData = this.createRequestWrapper(params);
        RequestBody requestBody = RequestBody.create(MediaType.parse(mediaTye), postData.toString());
        ((OdooServices)this.client.getServices()).getDataJsonCokies(this.sessionCokie, url, requestBody).enqueue(new Callback<OdooResponseDto<JsonElement>>() {
            public void onResponse(Call<OdooResponseDto<JsonElement>> call, Response<OdooResponseDto<JsonElement>> response) {
                if (response.errorBody() != null) {
                    try {
                        odooResponse.onResponse((Object)null, new Throwable(response.errorBody().string()));
                    } catch (IOException var5) {
                        odooResponse.onResponse((Object)null, var5);
                    }

                } else {
                    OdooClient.this.sessionCokie = response.headers().get("Set-Cookie");
                    System.out.println("success");
                    Type type = OObjectUtils.getType(odooResponse);
                    Object o = OdooClient.this.client.gson.fromJson((JsonElement)((OdooResponseDto)response.body()).result, type);
                    odooResponse.onResponse(o, (Throwable)null);
                }
            }

            public void onFailure(Call<OdooResponseDto<JsonElement>> call, Throwable t) {
                odooResponse.onResponse((Object)null, t);
            }
        });
    }

    public BaseClient<OdooServices> getClient() {
        return this.client;
    }

    public String getSessionCokie() {
        return this.sessionCokie;
    }
}