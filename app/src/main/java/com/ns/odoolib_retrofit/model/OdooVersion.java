//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.model;

import android.os.Bundle;

public class OdooVersion {
    public static final String TAG = OdooVersion.class.getSimpleName();
    private String serverSerie;
    private String serverVersion;
    private String versionType;
    private String versionRelease;
    private int versionNumber;
    private int versionTypeNumber;
    private Boolean isEnterprise = false;
    private String server_serie;
    private String server_version;
    private String version_type;
    private String version_release;
    private String[] server_version_info;

    public OdooVersion() {
    }

    public Bundle getAsBundle() {
        Bundle version = new Bundle();
        version.putString("server_serie", this.getServerSerie());
        version.putString("server_version", this.getServerVersion());
        version.putString("version_type", this.getVersionType());
        version.putString("version_release", this.getVersionRelease());
        version.putInt("version_number", this.getVersionNumber());
        version.putInt("version_type_number", this.getVersionTypeNumber());
        return version;
    }

    public int getVersionNumber() {
        return 13;
    }

    public void setVersionNumber(int versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String toString() {
        return "OdooVersion{serverSerie='" + this.serverSerie + '\'' + ", serverVersion='" + this.serverVersion + '\'' + ", versionType='" + this.versionType + '\'' + ", versionRelease='" + this.versionRelease + '\'' + ", versionNumber=" + this.versionNumber + ", versionTypeNumber=" + this.versionTypeNumber + ", isEnterprise=" + this.isEnterprise + '}';
    }

    public String getServerSerie() {
        return this.serverSerie;
    }

    public String getServerVersion() {
        return this.serverVersion;
    }

    public String getVersionType() {
        return this.versionType;
    }

    public String getVersionRelease() {
        return this.versionRelease;
    }

    public int getVersionTypeNumber() {
        return this.versionTypeNumber;
    }

    public Boolean getIsEnterprise() {
        return this.isEnterprise;
    }

    public String getServer_serie() {
        return this.server_serie;
    }

    public String getServer_version() {
        return this.server_version;
    }

    public String getVersion_type() {
        return this.version_type;
    }

    public String getVersion_release() {
        return this.version_release;
    }

    public String[] getServer_version_info() {
        return this.server_version_info;
    }

    public void setServerSerie(String serverSerie) {
        this.serverSerie = serverSerie;
    }

    public void setServerVersion(String serverVersion) {
        this.serverVersion = serverVersion;
    }

    public void setVersionType(String versionType) {
        this.versionType = versionType;
    }

    public void setVersionRelease(String versionRelease) {
        this.versionRelease = versionRelease;
    }

    public void setVersionTypeNumber(int versionTypeNumber) {
        this.versionTypeNumber = versionTypeNumber;
    }

    public void setIsEnterprise(Boolean isEnterprise) {
        this.isEnterprise = isEnterprise;
    }

    public void setServer_serie(String server_serie) {
        this.server_serie = server_serie;
    }

    public void setServer_version(String server_version) {
        this.server_version = server_version;
    }

    public void setVersion_type(String version_type) {
        this.version_type = version_type;
    }

    public void setVersion_release(String version_release) {
        this.version_release = version_release;
    }

    public void setServer_version_info(String[] server_version_info) {
        this.server_version_info = server_version_info;
    }
}