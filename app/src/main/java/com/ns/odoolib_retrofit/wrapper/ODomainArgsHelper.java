//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.wrapper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;

public class ODomainArgsHelper<T> {
    public static final String TAG = ODomainArgsHelper.class.getSimpleName();
    protected List<Object> mObjects = new ArrayList();

    public ODomainArgsHelper() {
    }

    public T add(Object data) {
        this.mObjects.add(data);
        return (T) this;
    }

    public T append(ODomain domain) {
        if (domain != null) {
            Iterator var2 = domain.getObject().iterator();

            while(var2.hasNext()) {
                Object obj = var2.next();
                this.add(obj);
            }
        }

        return (T) this;
    }

    public List<Object> getObject() {
        return this.mObjects;
    }

    public JSONArray getArray() {
        JSONArray result = new JSONArray();
        Iterator var2 = this.mObjects.iterator();

        while(var2.hasNext()) {
            Object obj = var2.next();
            result.put(obj);
        }

        return result;
    }

    public List<Object> getAsList() {
        return this.mObjects;
    }

    public JSONArray listToArray(Object collection) {
        JSONArray array = new JSONArray();
        List list = (List)collection;

        try {
            Iterator var4 = list.iterator();

            while(var4.hasNext()) {
                Object data = var4.next();
                array.put(data);
            }
        } catch (Exception var6) {
        }

        return array;
    }
}