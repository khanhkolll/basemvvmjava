//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.model;

public class OdooErrorV2 {
    private int code = -1;
    private String message;
    private OdooErrorDetail data;

    public OdooErrorV2() {
    }

    public String toString() {
        return "{\"message\":\"" + this.message + '"' + ", \"serverTrace\":" + this.data + '}';
    }

    public int getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    public OdooErrorDetail getData() {
        return this.data;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(OdooErrorDetail data) {
        this.data = data;
    }
}