//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

public class AdapterBindings {
    public AdapterBindings() {
    }

    @BindingAdapter({"app:recycleViewItems"})
    public static void setItems(RecyclerView listView, List<BaseModel> items) {
        BaseAdapter adapter = (BaseAdapter)listView.getAdapter();
        if (adapter != null) {
            adapter.replaceData(items);
        }

    }
}