//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ns.odoolib_retrofit.adapter;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.ns.odoolib_retrofit.model.OdooRelType;
import java.lang.reflect.Type;

public class MySerializer<T> implements JsonSerializer<T> {
    public MySerializer() {
    }

    public JsonElement serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
        if (typeOfSrc == OdooRelType.class) {
            OdooRelType odooRelType = (OdooRelType)src;
            if (odooRelType != null && odooRelType.size() >= 1) {
                Long id = Long.parseLong((String)odooRelType.get(0));
                return new JsonPrimitive(id);
            } else {
                return null;
            }
        } else {
            Gson gson = new Gson();
            JsonElement element = gson.toJsonTree(src, typeOfSrc);
            return element;
        }
    }
}