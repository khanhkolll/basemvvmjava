package com.kna.basemvvmjava.socket_notification;

public interface SocketIONavigationProvider {
    SocketIONavigation socketNavigation();
}
