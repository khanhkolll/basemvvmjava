package com.kna.basemvvmjava.service.notification_service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kna.basemvvmjava.R;
import com.kna.basemvvmjava.base.AppController;
import com.kna.basemvvmjava.base.Constants;
import com.kna.basemvvmjava.model.NotificationModel;
import com.ns.odoolib_retrofit.adapter.Adapter;
import com.ns.odoolib_retrofit.utils.OdooDateTime;

import org.greenrobot.eventbus.EventBus;

import java.util.Objects;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public static String NOTIFICATION_CHANNEL_ID = "share_vans_notification";
    String TAG = "NOTIFICATION_FIREBASE_FROM";

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        // phương thức được gọi khi đang  mở app
        Log.d(TAG, remoteMessage.getFrom());

        //final ObjectMapper mapper = new ObjectMapper();
        final NotificationService notificationService = NotificationService.of(remoteMessage.getData());//mapper.convertValue(remoteMessage.getData(), NotificationService.class);
        hideIcVansMapsFragment(notificationService);
        EventBus.getDefault().post(notificationService);
        if (notificationService != null) {
            sendNotification(notificationService);
        }
    }

    private void hideIcVansMapsFragment(NotificationService notificationService) {

    }

    private void sendNotification(NotificationService notification) {
        // hide vehicle when completed all routing
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(OdooDateTime.class, Adapter.DATETIME)
                .create();
        NotificationModel notificationModel = gson.fromJson(notification.getMess_object(), NotificationModel.class);

        Intent notificationIntent = new Intent(notification.getClick_action());

        if (notification.getItem_id() != null) {
            notificationIntent.putExtra(Constants.ITEM_ID, "" + notification.getItem_id());
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        //
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant")
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_MAX);
            // Configure the notification channel.
            notificationChannel.setDescription(notification.getBody());
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
//                .setSound(Uri.parse("android.resource://"
//                        + getPackageName() + "/" + R.raw.notification_bell))
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentTitle(notification.getTitle())
                .setContentIntent(pendingIntent)
                .setContentText(notification.getBody());
        assert notificationManager != null;
        notificationManager.notify(1, notificationBuilder.build());
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
    }
}
