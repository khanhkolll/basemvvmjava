//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.tsolution.base.databinding;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingComponent;

import com.kna.basemvvmjava.R;
import com.tsolution.base.BR;
import com.tsolution.base.BaseViewModel;

public class CommonActivityBindingImpl extends CommonActivityBinding {
    @Nullable
    private static final IncludedLayouts sIncludes = null;
    @Nullable
    private static final SparseIntArray sViewsWithIds = new SparseIntArray();

    static {
        sViewsWithIds.put(R.id.commonFrag, 1);
    }

    @NonNull
    private final LinearLayout mboundView0;
    private long mDirtyFlags;

    public CommonActivityBindingImpl(@Nullable DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 2, sIncludes, sViewsWithIds));
    }

    private CommonActivityBindingImpl(DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0, (FrameLayout) bindings[1]);
        this.mDirtyFlags = -1L;
        this.mboundView0 = (LinearLayout) bindings[0];
        this.mboundView0.setTag((Object) null);
        this.setRootTag(root);
        this.invalidateAll();
    }

    public void invalidateAll() {
        synchronized (this) {
            this.mDirtyFlags = 2L;
        }

        this.requestRebind();
    }

    public boolean hasPendingBindings() {
        synchronized (this) {
            return this.mDirtyFlags != 0L;
        }
    }

    public boolean setVariable(int variableId, @Nullable Object variable) {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            this.setViewModel((BaseViewModel) variable);
        } else {
            variableSet = false;
        }

        return variableSet;
    }

    public void setViewModel(@Nullable BaseViewModel ViewModel) {
        this.mViewModel = ViewModel;
    }

    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        return false;
    }

    protected void executeBindings() {
        long dirtyFlags = 0L;
        synchronized (this) {
            dirtyFlags = this.mDirtyFlags;
            this.mDirtyFlags = 0L;
        }
    }
}