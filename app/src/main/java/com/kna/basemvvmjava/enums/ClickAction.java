package com.kna.basemvvmjava.enums;

public class ClickAction {
    public static final String ROUTING="com.ts.sharevandriver.TARGET_ROUTING_PLAN_DAY";
    public static final String MAIN_ACTIVITY="com.ts.sharevandriver.TARGET_MAIN_ACTIVITY";
    public static final String VEHICLE_INFO="com.ts.sharevandriver.TARGET_VEHICLE_INFO";
    public static final String DRIVER_INFO="com.ts.sharevandriver.TARGET_CUSTOMER_RATED_TO_DRIVER";
    public static final String BIDDING_DETAIL="com.ts.sharevandriver.TARGET_BIDDING_DETAIL";
    public static final String ROUTING_HISTORY="com.ts.sharevandriver.TARGET_ROUTING_HISTORY";
    public static final String ROUTING_DETAIL="com.ts.sharevandriver.TARGET_ROUTING_DETAIL";
}
